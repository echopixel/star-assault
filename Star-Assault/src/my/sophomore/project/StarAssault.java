package my.sophomore.project;

import my.sophomore.project.controllers.LevelList;
import my.sophomore.project.entity.Player;
import my.sophomore.project.gamescreens.DiedScreen;
import my.sophomore.project.gamescreens.GameOverScreen;
import my.sophomore.project.gamescreens.GamePlayScreen;
import my.sophomore.project.gamescreens.LevelSelectScreen;
import my.sophomore.project.gamescreens.MainScreen;
import my.sophomore.project.gamescreens.PauseScreen;
import my.sophomore.project.gamescreens.SureScreen;
import my.sophomore.project.level.Level.Name;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class StarAssault extends Game
{

	private Player aria;
	public World world;
	
	private Name levelName;
	private MainScreen main;
	private GamePlayScreen play;
	private PauseScreen pause;
	private SureScreen sure;
	private LevelSelectScreen select;
	private GameOverScreen over;
	private DiedScreen died;
	
	
	private boolean android;
	
	private LevelList list;
	
	private boolean debugMode;
	
	@Override
	public void create()
	{
		android = !Gdx.input.isPeripheralAvailable(Peripheral.HardwareKeyboard);
		
		aria = new Player(null, this);
		world = new World(new Vector2(0, -30), false);
		main = new MainScreen(this);
		pause = new PauseScreen(this);
		select = new LevelSelectScreen(this);
		over = new GameOverScreen(this);
		died = new DiedScreen(this);
		
		list = new LevelList(this);
		
		debugMode = false;
		
		pause.pause();
		main.pause();
		
		setScreen(main);
	}
	
	public DiedScreen getDied()
	{
		return died;
	}
	
	public boolean getAndroid()
	{
		return android;
	}
	
	public void newAria()
	{
		aria = new Player(null, this);
	}
	
	public GameOverScreen getOver()
	{
		return over;
	}
	
	public Player getAria()
	{
		return aria;
	}
	
	public World getWorld()
	{
		return world;
	}
	
	public MainScreen getMain()
	{
		return main;
	}
	
	public PauseScreen getPause()
	{
		return pause;
	}
	
	public GamePlayScreen getPlay()
	{
		return play;
	}
	
	public SureScreen getSure()
	{
		return sure;
	}
	
	public LevelSelectScreen getSelect()
	{
		return select;
	}
	
	public Name getLevelName()
	{
		return levelName;
	}
	
	public LevelList getList()
	{
		return list;
	}
	
	public boolean getDebug()
	{
		return debugMode;
	}
	
	/**
	 * Pauses the current screen, resumes and changes to the next one
	 * 
	 * @param current The current screen the game is on
	 * @param next The screen you want the game to go to
	 */
	public void changeScreen(Screen current, Screen next)
	{
		current.pause();
		next.resume();
		setScreen(next);
	}
	
	public void newList()
	{
		list = new LevelList(this);
	}
	
	public void newWorld()
	{
		world = new World(new Vector2(0, -30), false);
	}
	
	public void newSelect()
	{
		select = new LevelSelectScreen(this);
	}
	
	public void newPlay(Name level, Player aria)
	{
		play = new GamePlayScreen(this, level, aria);
	}
	
	public void newSure(Boolean bool, Screen screen)
	{
		sure = new SureScreen(this, screen, bool);
	}

	public void changeDebug()
	{
		if(debugMode == true)
		{
			debugMode = false;
		}
		else
		{
			debugMode = true;
		}
		
		System.out.println("Debug Mode = " + debugMode);
	}
}
