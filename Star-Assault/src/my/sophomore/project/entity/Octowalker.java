package my.sophomore.project.entity;

import my.sophomore.project.StarAssault;
import my.sophomore.project.gamescreens.GamePlayScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Octowalker extends Enemy{
	
	public Octowalker( World world, Vector2 spawnPosition, StarAssault game) {
		super(world, spawnPosition, game);
		this.world = world;
		this.game = game;
		this.spawnPosition = spawnPosition;
		position = new Vector2();

		// Stats
		HP = 6;
		Power = 2;
		onScreen = false;
		moveSpeed = 1.5f;
		
		// Load up the textures and anims.
		texture = new Texture(Gdx.files.internal("data/sprites/octowalker.png"));
		TextureRegion[][] regions = TextureRegion.split(texture, 32, 32);
		frames = new TextureRegion[9];
		for (int x = 0; x <= 8; x++)
		{
			frames[x] = regions[0][x];
		}
		animSpeed = .15f;
		anim = new Animation(animSpeed, frames);
		
		BodyDef octoBodyDef = new BodyDef();
		octoBodyDef.type = BodyDef.BodyType.DynamicBody;
		octoBodyDef.position.set(spawnPosition);

		body = this.world.createBody(octoBodyDef);
		
		/**
		 * Boxes are defined by their "half width" and "half height", hence the
		 * 2 multiplier.
		 */
		PolygonShape octoShape = new PolygonShape();
		octoShape.setAsBox(0.2f, 0.25f);

		/**
		 * The character should not ever spin around on impact.
		 */
		body.setFixedRotation(true);
		/**
		 * The density and friction of the jumper were found experimentally.
		 * Play with the numbers and watch how the character moves faster or
		 * slower.
		 */
		FixtureDef octoFixtureDef = new FixtureDef();
		octoFixtureDef.shape = octoShape;
		octoFixtureDef.density = 6f;
		octoFixtureDef.friction = 0.1f;
		octoFixtureDef.restitution = 0;
		octoFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_MONSTER;
		octoFixtureDef.filter.maskBits = GamePlayScreen.MASK_MONSTER;
		fixture = body.createFixture(octoFixtureDef);
		fixture.setUserData(this);
		
		body.setLinearDamping(2.0f);
		octoShape.dispose();
	}

	@Override
	public void update(OrthographicCamera cam) {
		
		boolean moveLeft = false;
		boolean moveRight = false;
		boolean doJump = false;
		
	}

	@Override
	public void draw(SpriteBatch spriteBatch) {
		if (body != null) {
		currentFrameTime += Gdx.graphics.getDeltaTime();
		
		TextureRegion frame = anim.getKeyFrame(currentFrameTime, true);
		position.x = GamePlayScreen.PIXELS_PER_METER * body.getPosition().x
				- frames[0].getRegionWidth() / 2;
		position.y = GamePlayScreen.PIXELS_PER_METER * body.getPosition().y
				- (frames[0].getRegionHeight() / 2) +.25f;
		
		if (facingLeft == true)
		{
			position.x = GamePlayScreen.PIXELS_PER_METER * body.getPosition().x
					+ frames[0].getRegionWidth() / 2;
		}
		
		spriteBatch.draw(frame, position.x, position.y, 16, 16, 32f, 32f,
				0.2f, 0.2f, 0);
		}
	}
	
	@Override
	public void dispose(World world) {
		texture.dispose();
		world.destroyBody(body);
	}
	
	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public int getPower() {
		return Power;
	}
	
	@Override
	public int getHP() {
		return HP;
	}

	@Override
	public void setHP(int amount) {
		HP += amount;
	}

}
