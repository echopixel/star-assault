package my.sophomore.project.entity;

import my.sophomore.project.StarAssault;
import my.sophomore.project.gamescreens.GamePlayScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class FinishStar {

	Vector2 spawnPosition;
	Vector2 position;
	
	private Body finishBody;
	private Fixture finishFixture;
	
	private ParticleEffect finishParticle;
	private Texture finishTexture;
	private Sprite finishSprite;
	
	public FinishStar (World world, StarAssault game, Vector2 spawnPosition) {
		this.spawnPosition = spawnPosition;
		position = new Vector2();
		
		finishParticle = new ParticleEffect();
		finishParticle.load(Gdx.files.internal("data/particles/finishParticles.particle"), Gdx.files.internal("data/sprites"));
		
		finishTexture = new Texture(Gdx.files.internal("data/sprites/star.png"));
		finishSprite = new Sprite(finishTexture);
		
		BodyDef finishBodyDef = new BodyDef();
		finishBodyDef.type = BodyDef.BodyType.DynamicBody;
		finishBodyDef.position.set(spawnPosition);

		finishBody = world.createBody(finishBodyDef);
		
		/**
		 * Boxes are defined by their "half width" and "half height", hence the
		 * 2 multiplier.
		 */
		PolygonShape finishShape = new PolygonShape();
		finishShape.setAsBox(finishSprite.getWidth() / (10f * GamePlayScreen.PIXELS_PER_METER),
						   (finishSprite.getHeight() / (10f * GamePlayScreen.PIXELS_PER_METER)));

		/**
		 * The character should not ever spin around on impact.
		 */
		finishBody.setFixedRotation(true);
		finishBody.setGravityScale(0);
		/**
		 * The density and friction of the jumper were found experimentally.
		 * Play with the numbers and watch how the character moves faster or
		 * slower.
		 */
		FixtureDef finishFixtureDef = new FixtureDef();
		finishFixtureDef.isSensor = true;
		finishFixtureDef.shape = finishShape;
		finishFixtureDef.density = 6f;
		finishFixtureDef.friction = 0;
		finishFixtureDef.restitution = 0;
		finishFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_SCENERY;
		finishFixtureDef.filter.maskBits = GamePlayScreen.MASK_SCENERY;
		finishFixture = finishBody.createFixture(finishFixtureDef);
		finishFixture.setUserData(this);
	
		finishShape.dispose();
		
	}
	
	public void draw(SpriteBatch spriteBatch) {
		position.x = finishBody.getPosition().x * 10;
		position.y = finishBody.getPosition().y * 10;
		finishParticle.start();
		finishParticle.setPosition(position.x, position.y);
		finishParticle.draw(spriteBatch, Gdx.graphics.getDeltaTime());
		spriteBatch.draw(finishSprite, position.x - finishSprite.getWidth() / 4f, position.y - finishSprite.getHeight() / 4f
				, 0, 0, 32, 32, 0.5f, 0.5f, 0);
	}
}
