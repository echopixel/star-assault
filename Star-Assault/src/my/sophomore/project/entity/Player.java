package my.sophomore.project.entity;

import java.util.ArrayList;
import java.util.Random;

import my.sophomore.project.StarAssault;
import my.sophomore.project.gamescreens.GamePlayScreen;
import my.sophomore.project.level.Level.Name;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Player
{
	public enum Collision {
		UP, DOWN, LEFT, RIGHT
	}
	private enum State
	{
		IDLE, WALKING, JUMPING, DYING, ATTACKING
	}
	
	private float currentFrameTime;
	private float stateTime;
	private float recoveryTimer;
	private float wallJumpTimer;
	
	private StarAssault game;
	
	private int HP;
	private int Lives;
    private boolean onGround;
    private float wallSlideSpeed;
    private boolean canWallJump;
    private boolean doWallJump;
    private int wallJumps;
    private float wallJumpTime;
    private Vector2 wallJumpNormal;
	private boolean hasDoubleJump;
	private boolean hasWallJump;
	private int Power;
	private int numJumps;
	private State state;
	private World world;
	private Vector2 spawnPosition;
	private boolean hit;
	private float recoveryTime;
	private float starSpeed;

	private Collision currentCollision;
	
	private int multiShot;
	
	private Body ariaBody;
	private Fixture ariaFixture;
	
	private ArrayList<AriaProjectile> _projectiles;
	private ArrayList<AriaProjectile> _inactiveProjectiles;
	
	/**
	 * All of Aria's representation
	 */
	private Animation currentAnim;
	private Texture ariaIdleTexture;
	private Texture ariaWalkingTexture;
	private Texture ariaJumpingTexture;
	private Texture ariaWallSlideTexture;
	private TextureRegion[] idleTapFrames;
	private TextureRegion[] idleBlinkFrames;
	private TextureRegion[] walkingFrames;
	private TextureRegion[] jumpingFrames;
	private TextureRegion[] wallSlideFrames;
	private Animation ariaIDLETap;
	private Animation ariaIDLEBlink;
	private Animation ariaWalking;
	private Animation ariaJumping;
	private Animation ariaWallSliding;
	private boolean ariaFacingLeft;
	private Vector2 position;
	private boolean blinkBool;
	private Random rand;
	private float idleChance;
	private ParticleEffect jumpParticle;
	
	private float jumpImpulse;
	private float moveSpeed;
	
	private int camX;
	private int camY;
	
	private static final float BLINKMINSPEED = 0.6f;
	private static final float BLINKMAXSPEED = 0.7f;
	private static final float FOOTTAP_SPEED = 1.5f;
	
	public Player(World world, StarAssault game) 
	{		
		this.game = game;
		
		rand = new Random();
		
		// Load default stats
		HP = 20;
		Lives = 3;
		onGround = false;
		canWallJump = false;
		doWallJump = false;
		wallSlideSpeed = -1f;  //Negative because it relates to the y axis.
		wallJumps = 0;
		wallJumpNormal = new Vector2();
		hasDoubleJump = false;
		hasWallJump = false;
		currentCollision = currentCollision.DOWN;
		Power = 1;
		numJumps = 0;
		state = State.IDLE;
		position = new Vector2();
		jumpImpulse = 12.8f;
		moveSpeed = 2.35f;
		recoveryTime = 1f;
		wallJumpTime = 0.2f;
		_projectiles = new ArrayList<AriaProjectile>();
		_inactiveProjectiles = new ArrayList<AriaProjectile>();
		starSpeed = 10f;
		jumpParticle = new ParticleEffect();
		jumpParticle.load(Gdx.files.internal("data/particles/jumpParticles.particle"), 
				Gdx.files.internal("data/sprites"));
		
		multiShot = 3;
		
		
		/**
		 * Load up the texture and anims.
		 */
		ariaIdleTexture = new Texture(Gdx.files.internal("data/sprites/ariaIDLE.png"));
		TextureRegion[][] idleRegions = TextureRegion.split(ariaIdleTexture, 32, 32);
		idleTapFrames = idleRegions[0];
		idleBlinkFrames = idleRegions[1];
		ariaIDLETap = new Animation(FOOTTAP_SPEED, idleTapFrames);
		ariaIDLEBlink = new Animation(BLINKMAXSPEED, idleBlinkFrames);
		
		ariaWalkingTexture = new Texture(Gdx.files.internal("data/sprites/ariaWalk.png"));
		TextureRegion[][] walkingRegions = TextureRegion.split(ariaWalkingTexture, 32, 32);
		walkingFrames = walkingRegions[0];
		ariaWalking = new Animation(.30f, walkingFrames);
		
		ariaJumpingTexture = new Texture(Gdx.files.internal("data/sprites/ariaJump.png"));
		TextureRegion[][] jumpingRegions = TextureRegion.split(ariaJumpingTexture, 32, 32);
		jumpingFrames = jumpingRegions[0];
		ariaJumping = new Animation(1f, jumpingFrames);
		
		ariaWallSlideTexture = new Texture(Gdx.files.internal("data/sprites/ariaWallSlide.png"));
		TextureRegion[][] wallSlideRegions = TextureRegion.split(ariaWallSlideTexture, 32,32);
		wallSlideFrames = wallSlideRegions[0];
		ariaWallSliding = new Animation(1f, wallSlideFrames);
		
		currentAnim = ariaIDLETap;
		currentFrameTime = 0.0f;
		
		ariaBody = null;
	
	}
	
	public void setWallJump(boolean bool)
	{
		hasWallJump = bool;
	}
	
	public void resetHP()
	{
		HP = 20;
	}
	
	public int getMulti()
	{
		return multiShot;
	}
	
	public void resetMulti()
	{
		multiShot = 3;
	}
	
	public void update(OrthographicCamera cam, World world) 
	{
			Power = 2;
		
		//System.out.println(hit + "   " + recoveryTimer);
		System.out.println(canWallJump);
		//System.out.println(ariaBody.getLinearVelocity());
		//System.out.println(currentCollision);

		if (hit == true && recoveryTimer >= recoveryTime) {
			recoveryTimer = 0;
			hit = false;
		}
		
		if(game.getPlay().getLevelName() == Name.ONE)
		{
			camX = 200;
			camY = 30;
		}
		
		if(game.getPlay().getLevelName() == Name.TWO)
		{
			camX = 150;
			camY = 50;
		}
		
		if(game.getPlay().getLevelName() == Name.THREE)
		{
			camX = 200;
			camY = 200;
		}
		
		if(game.getPlay().getLevelName() == Name.FOUR)
		{
			camX = 50;
			camY = 50;
		}
		
		camX = (camX * 4) - 84;
		camY = (camY * 4) - 50;
		
		if (!Gdx.input.isKeyPressed(Keys.A) && !Gdx.input.isKeyPressed(Keys.S) && !Gdx.input.isKeyPressed(Keys.W)
				&& !Gdx.input.isKeyPressed(Keys.COMMA)
				&& !Gdx.input.isKeyPressed(Keys.PERIOD)
				&& !Gdx.input.isKeyPressed(Keys.D)) {
			if ((ariaIDLEBlink.isAnimationFinished(stateTime)
				|| ariaIDLETap.isAnimationFinished(stateTime)))
			this.idleUpdate();
			
			else {
				currentAnim = ariaIDLETap;
			}
		}
		
		if (canWallJump == true) {
			currentAnim = ariaWallSliding;
		}
		else if (numJumps > 0 || wallJumps > 0) {
			currentAnim = ariaJumping;
		}
		
		
		stateTime += Gdx.graphics.getDeltaTime();
		
		if (hit == true) {
		recoveryTimer += Gdx.graphics.getDeltaTime();
		}
		
		if (doWallJump == true) {
			wallJumpTimer += Gdx.graphics.getDeltaTime();
			canWallJump = false;
			onGround = false;
			if (wallJumpTimer >= wallJumpTime) {
				doWallJump = false;
				wallJumpTimer = 0;
				wallJumps = 0;
			}
		}
		
		
		this.moveUpdate();

        this.camUpdate(cam);
	}
	
	
	
	
	/**
	 * Controls the movement updates for Aria.
	 * Includes input, player movement and animation updates.
	 */
	public void moveUpdate() {
		/**
		 * Detect requested motion.
		 */
		boolean moveLeft = false;
		boolean moveRight = false;
		boolean doJump = false;
		
		/*
		 * KEYBOARD CONTROLS
		 */
		Gdx.input.setInputProcessor(new InputAdapter()
		{

			@Override
		    public boolean keyDown(int keycode)
		    {
				if (doWallJump == false) {
				
				
		    	if(keycode == Keys.J && game.getDebug())
		    	{
		    		if(hasDoubleJump == false)
		    		{
		    			hasDoubleJump = true;
		    		}
		    		else
		    		{
		    			hasDoubleJump = false;
		    		}
		    	}
		    	
		    	if(keycode == Keys.K && game.getDebug())
		    	{
		    		if(hasWallJump == false)
		    		{
		    			hasWallJump = true;
		    		}
		    		else
		    		{
		    			hasWallJump = false;
		    		}
		    	}
		    	
		    	if(keycode == Keys.M && game.getDebug())
		    	{
		    		game.getPlay().getRenderer().changeDebug();
		    	}
		    	
		    	if (keycode == Keys.PERIOD) {
		    		if (ariaFacingLeft == false) {
		    			starSpeed = Math.abs(starSpeed);
		    		}
		    		if (ariaFacingLeft == true) {
		    			starSpeed = Math.abs(starSpeed) * -1;
		    		}
		    		
		    		if(_inactiveProjectiles.size() >= 1)
		    		{
	    				AriaProjectile projectile = _inactiveProjectiles.get(0);
	    				_projectiles.add(_inactiveProjectiles.get(0));
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setTransform(ariaBody.getPosition(), 0);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setActive(true);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setLinearVelocity(starSpeed, 0);
	    				
	    				_inactiveProjectiles.remove(_inactiveProjectiles.get(_inactiveProjectiles.indexOf(projectile)));
	    			}
		    	
		    		
		    	}
		    	
		    	if (keycode == Keys.COMMA) {
		    		if (ariaFacingLeft == false) {
		    			starSpeed = Math.abs(starSpeed);
		    		}
		    		if (ariaFacingLeft == true) {
		    			starSpeed = Math.abs(starSpeed) * -1;
		    		}
		    		
		    		if(_inactiveProjectiles.size() >= 5 && multiShot >= 1)
		    		{
	    				
	    				AriaProjectile projectile = _inactiveProjectiles.get(0);
	    				_projectiles.add(_inactiveProjectiles.get(0));
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setTransform(ariaBody.getPosition(), -.4f);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setActive(true);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setLinearVelocity(starSpeed, -.4f);
	    				
	    				_inactiveProjectiles.remove(_inactiveProjectiles.get(_inactiveProjectiles.indexOf(projectile)));
	    				
	    				projectile = _inactiveProjectiles.get(0);
	    				_projectiles.add(_inactiveProjectiles.get(0));
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setTransform(ariaBody.getPosition(), -.2f);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setActive(true);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setLinearVelocity(starSpeed, -.2f);
	    				
	    				_inactiveProjectiles.remove(_inactiveProjectiles.get(_inactiveProjectiles.indexOf(projectile)));
	    				
	    				projectile = _inactiveProjectiles.get(0);
	    				_projectiles.add(_inactiveProjectiles.get(0));
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setTransform(ariaBody.getPosition(), .4f);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setActive(true);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setLinearVelocity(starSpeed, .4f);
	    				
	    				_inactiveProjectiles.remove(_inactiveProjectiles.get(_inactiveProjectiles.indexOf(projectile)));
	    				
	    				projectile = _inactiveProjectiles.get(0);
	    				_projectiles.add(_inactiveProjectiles.get(0));
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setTransform(ariaBody.getPosition(), .2f);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setActive(true);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setLinearVelocity(starSpeed, .2f);
	    				
	    				_inactiveProjectiles.remove(_inactiveProjectiles.get(_inactiveProjectiles.indexOf(projectile)));
	    				
	    				projectile = _inactiveProjectiles.get(0);
	    				_projectiles.add(_inactiveProjectiles.get(0));
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setTransform(ariaBody.getPosition(), 0);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setActive(true);
	    				_projectiles.get(_projectiles.indexOf(projectile)).getProjectileBody().setLinearVelocity(starSpeed, 0);
	    				
	    				_inactiveProjectiles.remove(_inactiveProjectiles.get(_inactiveProjectiles.indexOf(projectile)));
	    				
	    				multiShot--;
	    			}
		    	
		    		
		    	}
		    	
			    	if (keycode == Keys.W && canWallJump == true && wallJumps < 1 && hit == false) {
			    		
			    		
			    		System.out.println("Wall Jump");
			    		wallJumps++;
			        	doWallJump = true;
			        	canWallJump = false;
			        	
			        	jumpParticle.start();
			        	ariaBody.applyForceToCenter(new Vector2(150f * wallJumpNormal.x, 800f));
			        }
		    	
		    	//previous.y <= ariaBody.getLinearVelocity().y + 1e-2
		    	
		        if (keycode == Keys.W && doWallJump == false && onGround == true && hit == false)
		        {
		        	System.out.println("Jump");
		        	onGround = false;
		        	ariaBody.applyLinearImpulse(new Vector2(0.0f, jumpImpulse),
											ariaBody.getWorldCenter());
		        	numJumps++;
		        	return true;
		        }
		        
		        
		        
		        if (keycode == Keys.W && hasDoubleJump && doWallJump == false && numJumps < 2 && hit == false)
		        {
		        	System.out.println("DoubleJump");
		        	ariaBody.setLinearVelocity(new Vector2(ariaBody.getLinearVelocity().x, 12f));
		        	numJumps++;
		        	jumpParticle.start();
		        	return true;
		        }
		        
		        if(keycode == Keys.ESCAPE)
		        {
		        	game.getPlay().pause();
		        	game.changeScreen(game.getPlay(), game.getPause());
		        	return true;
		        }
				}
		        return false;
		    }
		    
		    @Override
		    public boolean keyUp(int keycode)
		    {
		    	if (doWallJump == false) {
		    		if (keycode == Keys.W && ariaBody.getLinearVelocity().y >= 0.5f) {
		        		ariaBody.setLinearVelocity(ariaBody.getLinearVelocity().x, 1f);
		        		return true;
		        	}
		    	}
		    	
		    	return false;
		    }
		});
		
		if (doWallJump == false) {
		/*
		 * TOUCH CONTROLS
		 */
		if (Gdx.input.isKeyPressed(Input.Keys.D))
		{
			currentAnim = ariaWalking;
			if (numJumps > 0 || wallJumps > 0) {
				currentAnim = ariaJumping;
			}
			if (canWallJump == true) {
				currentAnim = ariaWallSliding;
			}
			moveRight = true;
		}
		else
		{
			for (int i = 0; i < 2; i++)
			{
				if (Gdx.input.isTouched(i)
					&& Gdx.input.getX() > Gdx.graphics.getWidth() * 0.80f)
				{
					currentAnim = ariaWalking;
					if (numJumps > 0 || wallJumps > 0) {
						currentAnim = ariaJumping;
					}
					if (canWallJump == true) {
						currentAnim = ariaWallSliding;
					}
					moveRight = true;
				}
			}
		}

		if (Gdx.input.isKeyPressed(Input.Keys.A))
		{
			currentAnim = ariaWalking;
			if (numJumps > 0 || wallJumps > 0) {
				currentAnim = ariaJumping;
			}
			if (canWallJump == true) {
				currentAnim = ariaWallSliding;
			}
			moveLeft = true;
		}
		else
		{
			for (int i = 0; i < 2; i++)
			{
				if (Gdx.input.isTouched(i)
					&& Gdx.input.getX() < Gdx.graphics.getWidth() * 0.20f)
				{
					moveLeft = true;
				}
			}
		}
		
		if (Gdx.input.justTouched()
			&& Gdx.input.getY() < Gdx.graphics.getHeight() * 0.20f)
		{
			doJump = true;
		}
			
		/**
		 * Act on the requested motions.
		 * 
		 * This code changes Aria's direction based on the previous controls. 
		 * The horizontal figure was arrived at experimentally -- try other
		 * values to experience different speeds.
		 * 
		 * The impulses are applied to the center of Aria.
		 */
		if (doJump && onGround == true && hit == false)
		{
	        numJumps = 0;
	        ariaBody.applyLinearImpulse(new Vector2(0.0f, jumpImpulse),
										ariaBody.getWorldCenter());

	        numJumps++;
	        doJump = false;
		}
		else if (doJump && hasDoubleJump && numJumps < 2 && hit == false)
		{
	        ariaBody.setLinearVelocity(new Vector2(ariaBody.getLinearVelocity().x, 12f));
	        numJumps++;
	        doJump = false;
		}
		else if (moveRight)
		{
			
			if(hit == false) {
				
				ariaBody.setLinearVelocity(new Vector2(moveSpeed, ariaBody.getLinearVelocity().y));
				
				if (ariaFacingLeft == true)
				{
					idleTapFrames[0].flip(true, false);
					idleTapFrames[1].flip(true, false);
					idleBlinkFrames[0].flip(true, false);
					idleBlinkFrames[1].flip(true, false);
					walkingFrames[0].flip(true, false);
					walkingFrames[1].flip(true, false);
					walkingFrames[2].flip(true, false);
					walkingFrames[3].flip(true, false);
					jumpingFrames[0].flip(true, false);
					wallSlideFrames[0].flip(true, false);
				}
				
				ariaFacingLeft = false;
				
				if (canWallJump == true && ariaFacingLeft == false) {
					idleTapFrames[0].flip(true, false);
					idleTapFrames[1].flip(true, false);
					idleBlinkFrames[0].flip(true, false);
					idleBlinkFrames[1].flip(true, false);
					walkingFrames[0].flip(true, false);
					walkingFrames[1].flip(true, false);
					walkingFrames[2].flip(true, false);
					walkingFrames[3].flip(true, false);
					jumpingFrames[0].flip(true, false);
					wallSlideFrames[0].flip(true, false);
					ariaFacingLeft = true;
				}
			}
			
			if(hit == true)
			ariaBody.setAngularVelocity(ariaBody.getAngularVelocity());
				
			
		}
		
		else if (moveLeft) {
			
			if (hit == false) {
				ariaBody.setLinearVelocity(new Vector2(-moveSpeed, ariaBody.getLinearVelocity().y));
				
				if (ariaFacingLeft == false)
				{
					idleTapFrames[0].flip(true, false);
					idleTapFrames[1].flip(true, false);
					idleBlinkFrames[0].flip(true, false);
					idleBlinkFrames[1].flip(true, false);
					walkingFrames[0].flip(true, false);
					walkingFrames[1].flip(true, false);
					walkingFrames[2].flip(true, false);
					walkingFrames[3].flip(true, false);
					jumpingFrames[0].flip(true, false);
					wallSlideFrames[0].flip(true, false);
				}
				
				
				ariaFacingLeft = true;
				
				if (canWallJump == true && ariaFacingLeft == true) {
					idleTapFrames[0].flip(true, false);
					idleTapFrames[1].flip(true, false);
					idleBlinkFrames[0].flip(true, false);
					idleBlinkFrames[1].flip(true, false);
					walkingFrames[0].flip(true, false);
					walkingFrames[1].flip(true, false);
					walkingFrames[2].flip(true, false);
					walkingFrames[3].flip(true, false);
					jumpingFrames[0].flip(true, false);
					wallSlideFrames[0].flip(true, false);
					ariaFacingLeft = false;
				}
			}
			
			if (hit == true)
			ariaBody.setAngularVelocity(ariaBody.getAngularVelocity());
			
			
		} 
		else {
			
			if(hit == false) {
				if (doWallJump == false)
					ariaBody.setLinearVelocity(new Vector2(0.0f, ariaBody.getLinearVelocity().y));
				
				if (doWallJump == true)
					ariaBody.setLinearVelocity(new Vector2(ariaBody.getLinearVelocity().x, ariaBody.getLinearVelocity().y));
			}
			
			if (hit == true)
			ariaBody.setAngularVelocity(ariaBody.getAngularVelocity());
			
			}
		}
	}
	
	/**
	 * Controls the updating of Aria's camera.
	 * @param cam
	 */
	public void camUpdate(OrthographicCamera cam) {
		/*
         * Controls the world and parallax cameras that move
         * with the player.
         */
		cam.position.x = GamePlayScreen.PIXELS_PER_METER
				* ariaBody.getPosition().x;
		cam.position.y = GamePlayScreen.PIXELS_PER_METER
				* ariaBody.getPosition().y;
		
		if (cam.position.x < 84)
		{
			cam.position.x = 84;
		}
		
		if (cam.position.x > camX)
		{
			cam.position.x = camX;
		}
		
		if (cam.position.y < 60)
		{
			cam.position.y = 60;
		}
		
		if (cam.position.y > camY)
		{
			cam.position.y = camY;
		}
	}
	
	/**
	 * Handles the decisions on Aria's idle frames.
	 */
	public void idleUpdate() {
		
		idleChance = rand.nextFloat();
		
		
			if(idleChance > 0.7f)
			{
				blinkBool = rand.nextBoolean();
						
				if (blinkBool)
					{
						ariaIDLEBlink = new Animation(BLINKMAXSPEED, idleBlinkFrames);
					}
					else {
						ariaIDLEBlink = new Animation(BLINKMINSPEED, idleBlinkFrames);
					}
						
				currentAnim = ariaIDLEBlink;
			}
			else
			{
				currentAnim = ariaIDLETap;
			}
			stateTime = 0;
		
	}
	
	/**
	 * Handles the drawing of Aria's graphical representation.
	 * @param spriteBatch
	 */
	public void draw(SpriteBatch spriteBatch)
	{
		currentFrameTime += Gdx.graphics.getDeltaTime();
		
		
			for (AriaProjectile projectile : _projectiles) {
				if (projectile.getProjectileBody().isActive() == true)
					projectile.draw(spriteBatch);
			}
		
		
		TextureRegion frame = currentAnim.getKeyFrame(currentFrameTime, true);
		position.x = GamePlayScreen.PIXELS_PER_METER * ariaBody.getPosition().x
				- idleTapFrames[0].getRegionWidth() / 2;
		position.y = GamePlayScreen.PIXELS_PER_METER * ariaBody.getPosition().y
				- (idleTapFrames[0].getRegionHeight() / 2) +.25f;
		
		if (ariaFacingLeft == true)
		{
			position.x = GamePlayScreen.PIXELS_PER_METER * ariaBody.getPosition().x
					+ idleTapFrames[0].getRegionWidth() / 2;
		}
		
		jumpParticle.setPosition(position.x +15, position.y + 15);
		jumpParticle.draw(spriteBatch, Gdx.graphics.getDeltaTime());
			
		spriteBatch.draw(frame, position.x, position.y, 16, 16, 32f, 32f,
				0.25f, 0.25f, 0);
	}
	
	/**
	 * Sets up the box for the player to attach to as well as the spawn position for the level.
	 * MUST BE CALLED WHEN GOING TO A NEW LEVEL
	 * @param world Physics world the player resides in.
	 * @param spawnPosition Position to spawn player denoted by a Vector 2.
	 */
	public void setPlayerSpawn(World world, Vector2 spawnPosition)
	{
		this.spawnPosition = spawnPosition;
		
		if (!game.getWorld().isLocked()){
			
		_inactiveProjectiles.removeAll(_inactiveProjectiles);
		_projectiles.removeAll(_projectiles);
		createProjectiles();
		
		BodyDef ariaBodyDef = new BodyDef();
		ariaBodyDef.type = BodyDef.BodyType.DynamicBody;
		ariaBodyDef.position.set(spawnPosition);

		ariaBody = world.createBody(ariaBodyDef);
		
		/**
		 * Boxes are defined by their "half width" and "half height", hence the
		 * 2 multiplier.
		 */
		PolygonShape ariaShape = new PolygonShape();
		ariaShape.setAsBox(((idleTapFrames[0].getTexture().getWidth() / 8) - 2) / (4f * GamePlayScreen.PIXELS_PER_METER),
						   ((idleTapFrames[0].getTexture().getHeight() / 4) - 2.75f) / (4f * GamePlayScreen.PIXELS_PER_METER));

		/**
		 * The character should not ever spin around on impact.
		 */
		ariaBody.setFixedRotation(true);
		/**
		 * The density and friction of the jumper were found experimentally.
		 * Play with the numbers and watch how the character moves faster or
		 * slower.
		 */
		
		FixtureDef ariaFixtureDef = new FixtureDef();
		ariaFixtureDef.shape = ariaShape;
		ariaFixtureDef.density = 6f;
		ariaFixtureDef.friction = 0f;
		ariaFixtureDef.restitution = 0;
		ariaFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_PLAYER;
		ariaFixtureDef.filter.maskBits = GamePlayScreen.MASK_PLAYER;
		ariaFixture = ariaBody.createFixture(ariaFixtureDef);
		ariaFixture.setUserData(this);
		
		ariaBody.setLinearDamping(2.0f);
		ariaShape.dispose();
		}
	}
	
	public void dispose(World world) {
		
	}
		
	public State getState()
	{
		return state;
	}

	public void setState(State newState)
	{
		this.state = newState;
	}

	public int getLives() {
		return this.Lives;
	}

	public Vector2 getPosition() {
		return this.position;
	}
	
	public Vector2 getSpawnPosition() {
		return this.spawnPosition;
	}

	public int getHP() {
		return this.HP;
	}
	
	public Body getBody() {
		return this.ariaBody;
	}
	
	
	public ArrayList<AriaProjectile> getInactiveProjectiles() {
		return _inactiveProjectiles;
	}
	
	public ArrayList<AriaProjectile> getProjectiles() {
		return _projectiles;
	}
	
	public int getPower () {
		return Power;
	}
	
	public World getWorld() {
		return this.world;
	}
	
	public boolean getHasDoubleJump() {
		return hasDoubleJump;
	}
	
	public boolean getHasWallJump() {
		return hasWallJump;
	}
	
	public float getWallSlideSpeed() {
		return wallSlideSpeed;
	}
	
	public void setHasWallJump(Boolean bool) {
		hasWallJump = bool;
	}
	
	public boolean getCanWallJump() {
		return canWallJump;
	}
	
	public boolean getDoWallJump() {
		return doWallJump;
	}
	
	public boolean getOnGround() {
		return onGround;
	}
	
	public void setCanWallJump(Boolean bool) {
		canWallJump = bool;
	}
	
	public void resetWallJumps() {
		wallJumps = 0;
	}
	
	public void setWallJumpNormal(Vector2 normal) {
		wallJumpNormal = normal;
	}
	
	public void setDoubleJump(boolean bool)
	{
		hasDoubleJump = bool;
	}
	
	public void setOnGround(Boolean bool) {
		onGround = bool;
		numJumps = 0;
	}

	public void setHP(int i) {
		this.HP = this.HP + i;
	}

	public void setLives(int i) {
		this.Lives = this.Lives + i;
	}
	
	public void setWorld (World world) {
		this.world = world;
	}
	
	public void setHit (Boolean bool) {
		hit = bool;
	}
	
	public void setCurrentCollision (Collision collision) {
		currentCollision = collision;
	}
	
	public Collision getCurrentCollision () {
		return currentCollision;
	}
	
	
	public void createProjectiles() {
		for (int i = 0; i < 5; i++)
    	{
    		AriaProjectile projectile = new AriaProjectile(world, i);
    		
    		_inactiveProjectiles.add(projectile);
    	}
	}

	

}
