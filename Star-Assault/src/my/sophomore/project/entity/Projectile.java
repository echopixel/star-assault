package my.sophomore.project.entity;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Creates projectiles and bodies that follow physics that are set to them.
 * Ex. - Aria's stars or enemy energy shots, lobbed grenades, whatever.
 * @author Adam
 *
 */
public abstract class Projectile {
	// Set the gravity scale to zero so this body will float
	// bodyDef.gravityScale = 0.0f;
	
	// Make loop that will make it so that the sprite isn't drawn when false.
	// Set to true when it exists.   So, in all, will need three separate projectiles.
	// When goes off screen.	
	// projectile[x].setAwake(false);
	// When player hits fire key
	// projectile[x].setAwake(true);
	// if (ariaFacingLeft) {
	//	projectile[x].setTransform(aria.getPosition.x - x, aria.getPosition.y + y, 0);
	// }
	// else {
	// projectile[x].setTransform(aria.getPosition.x + x, aria.getPosition.y + y, 0);
	// }
	// make a for loop using last projectile fired to decide the next body to draw when firing.
	
	
	
	public Projectile (World world) {
	
	}
	
	public abstract void update(OrthographicCamera cam);
	
	public abstract void draw(SpriteBatch spriteBatch);
	
	public abstract void dispose(World world);

}
