package my.sophomore.project.entity;

import my.sophomore.project.gamescreens.GamePlayScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class AriaProjectile extends Projectile {

	private Texture starTexture;
	private Sprite starSprite;
	
	private Body starBody;
	private Fixture starFixture;
	private Vector2 position;
	private int rotate;
	private int id;
	
	public AriaProjectile(World world, int id) {
		super(world);
		this.id = id;
		position = new Vector2();
		
		starTexture = new Texture(Gdx.files.internal("data/sprites/star.png"));
		starSprite = new Sprite(starTexture);
		
		BodyDef starBodyDef = new BodyDef();
		starBodyDef.type = BodyDef.BodyType.DynamicBody;
		starBodyDef.active = false;
		starBodyDef.bullet = true;

		starBody = world.createBody(starBodyDef);
		starBody.setGravityScale(0);
		
		/**
		 * Boxes are defined by their "half width" and "half height", hence the
		 * 2 multiplier.
		 */
		PolygonShape starShape = new PolygonShape();
		starShape.setAsBox(0.15f, 0.15f);

		/**
		 * The character should not ever spin around on impact.
		 */
		starBody.setFixedRotation(true);
		/**
		 * The density and friction of the jumper were found experimentally.
		 * Play with the numbers and watch how the character moves faster or
		 * slower.
		 */
		FixtureDef starFixtureDef = new FixtureDef();
		starFixtureDef.shape = starShape;
		starFixtureDef.density = 0f;
		starFixtureDef.friction = 0;
		starFixtureDef.restitution = 0;
		starFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_PLAYER;
		starFixtureDef.filter.maskBits = GamePlayScreen.MASK_PLAYER;
		starFixture = starBody.createFixture(starFixtureDef);
		starFixture.setUserData(this);
		
		
		starShape.dispose();
	}

	@Override
	public void update(OrthographicCamera cam) {
		// TODO Auto-generated method stub
		
	}

	public Body getBody()
	{
		return starBody;
	}
	
	public int getId()
	{
		return id;
	}
	
	@Override
	public void draw(SpriteBatch spriteBatch) {
		if (starBody.isActive() == true) {
			//System.out.println(id);
		position.x = GamePlayScreen.PIXELS_PER_METER * starBody.getPosition().x - starSprite.getWidth() / 2;
		position.y = GamePlayScreen.PIXELS_PER_METER * starBody.getPosition().y - starSprite.getHeight() / 2;
		spriteBatch.draw(starSprite, position.x, position.y, 16, 16, 32f, 32f,
				0.25f, 0.25f, rotate);
		}
		rotate += 10;
		if(rotate > 360){
			rotate = 0;
		}
	}

	@Override
	public void dispose(World world) {
		// TODO Auto-generated method stub
		
	}
	
	public Body getProjectileBody() {
		return starBody;
	}

}
