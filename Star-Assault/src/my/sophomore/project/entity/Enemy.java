package my.sophomore.project.entity;

import java.util.Random;

import my.sophomore.project.StarAssault;
import my.sophomore.project.gamescreens.GamePlayScreen;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Base enemy class
 * @author Adam
 *
 */
public abstract class Enemy extends GamePlayScreen {

	StarAssault game;
	
	float currentFrameTime;
	
	int HP;
	boolean Dead;
	boolean onScreen;
	int Power;
	float moveSpeed;
	float attackSpeed;
	Random rand;
	PickUp pickUp;
	Vector2 spawnPosition;
	World world;
	
	Body body;
	Fixture fixture;
	
	Animation currentAnim;
	
	Texture texture;
	TextureRegion[] frames;
	Animation anim;
	boolean facingLeft;
	Vector2 position;
	float animSpeed;
	
	public Enemy(World world, Vector2 spawnPosition, StarAssault game) {
		
		super(game, game.getPlay().getLevelName(), game.getPlay().getLevel().aria);
		this.game = game;
		
	}
	
	public abstract void update(OrthographicCamera cam);
	
	public abstract void draw(SpriteBatch spriteBatch);
	
	public abstract void dispose(World world);
	
	public abstract Body getBody();
	
	public abstract int getPower();
	
	public abstract int getHP();
	
	public abstract void setHP(int amount);

}
