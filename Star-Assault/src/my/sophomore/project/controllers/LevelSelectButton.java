package my.sophomore.project.controllers;

import my.sophomore.project.gamescreens.LevelSelectScreen.LevelName;

public class LevelSelectButton
{
	
	private int x;
	private int y;
	private LevelName place;
	
	public LevelSelectButton(int x, int y, LevelName place)
	{
		this.x = x;
		this.y = y;
		this.place = place;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public LevelName getPlace()
	{
		return place;
	}
}
