package my.sophomore.project.controllers;

import my.sophomore.project.StarAssault;
import my.sophomore.project.level.Level.Name;

public class LevelList
{
	private StarAssault game;
	
	private boolean oneC;
	private boolean twoC;
	private boolean threeC;
	private boolean fourC;
	private boolean fiveC;
	private boolean lastC;
	
	public LevelList(StarAssault game)
	{
		this.game = game;
		
		oneC = false;
		twoC = false;
		threeC = false;
		fourC = false;
		fiveC = false;
		lastC = false;
	}

	public boolean getLevelComplet(Name name)
	{
		if(name == Name.ONE)
		{
			return oneC;
		}
		
		if(name == Name.TWO)
		{
			return twoC;
		}
		
		if(name == Name.THREE)
		{
			return threeC;
		}
		
		if(name == Name.FOUR)
		{
			return fourC;
		}
		
		if(name == Name.FIVE)
		{
			return fiveC;
		}
		
		if(name == Name.LAST)
		{
			return lastC;
		}
		
		return false;
	}
	
	public void setLevelComplete(Name name, boolean bool)
	{
		if(name == Name.ONE)
		{
			oneC = bool;
		}
		
		if(name == Name.TWO)
		{
			twoC = bool;
		}
		
		if(name == Name.THREE)
		{
			threeC = bool;
		}
		
		if(name == Name.FOUR)
		{
			fourC = bool;
		}
		
		if(name == Name.FIVE)
		{
			fiveC = bool;
		}
		
		if(name == Name.LAST)
		{
			lastC = bool;
		}
	}
}
