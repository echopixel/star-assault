package my.sophomore.project.gamescreens;

import my.sophomore.project.StarAssault;
import my.sophomore.project.entity.Player;
import my.sophomore.project.level.Level;
import my.sophomore.project.level.Level.Name;
import my.sophomore.project.level.LevelRenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;

public class GamePlayScreen implements Screen {
	
	private int i;
	private StarAssault game;
	private Level level;
	private LevelRenderer renderer;
	private Player aria;
	
	private Name levelName;
	
	// COLLISION MASKS SO ENEMIES CAN'T INTERACT WITH EACH OTHER.
	public static final short CATEGORY_PLAYER = 0x0001;  
	public static final short CATEGORY_MONSTER = 0x0002; 
	public static final short CATEGORY_SCENERY = 0x0004; 
	public static final short MASK_PLAYER = CATEGORY_MONSTER | CATEGORY_SCENERY; // or ~CATEGORY_PLAYER
	public static final short MASK_MONSTER = CATEGORY_PLAYER | CATEGORY_SCENERY; // or ~CATEGORY_MONSTER
	public static final short MASK_SCENERY = -1;
	
	public static final float PIXELS_PER_METER = 10.0f;

	public GamePlayScreen(StarAssault game, Name levelName, Player aria)
	{
		this.game = game;
		this.aria = aria;
		this.levelName = levelName;
		i = 0;
		game.getAria().resetMulti();
		
	}
	
	public LevelRenderer getRenderer()
	{
		return renderer;
	}
	
	public Level getLevel()
	{
		return level;
	}
	
	public Name getLevelName()
	{
		return levelName;
	}
	
	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		renderer.render();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		
		if(i == 0)
		{
			level = new Level(game, levelName, aria);
			renderer = new LevelRenderer(level, game);
			i++;
		}
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		//game.setScreen(new PauseScreen(game, this));
		
	}

	@Override
	public void resume() {
		
		
	}

	@Override
	public void dispose() {
		game.getPlay().getLevel().dispose(game.getWorld());
		game.getAria().dispose(game.getWorld());
	}

}
