package my.sophomore.project.gamescreens;

import my.sophomore.project.StarAssault;
import my.sophomore.project.controllers.OverlapTester;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class PauseScreen implements Screen
{
	private StarAssault game;
	
	private OrthographicCamera guiCam;
	
	private SpriteBatch spriteBatch;
	
	private Rectangle resumeBound;
	private Rectangle mainBound;
	private Rectangle quitBound;
	
	private Vector3 touchPoint;
	
	private Sprite resume;
	private Sprite main;
	private Sprite quit;
	private Texture background;
	private BitmapFont font;

	public PauseScreen (StarAssault game)
	{		
		this.game = game;

		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(400, 240, 0);
		
		spriteBatch = new SpriteBatch();
		
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		
		resumeBound = new Rectangle(435, 265, 128, 69);
		mainBound   = new Rectangle(435, 185, 128, 69);
		quitBound   = new Rectangle(435, 105, 128, 69);
		touchPoint  = new Vector3();
		
		background = new Texture(Gdx.files.internal(("data/backgrounds/pauseMenuEffects.png")));
		resume     = new Sprite(new Texture(Gdx.files.internal("data/GUI/resumeGame.png")), 0, 0, 128, 69);
		quit       = new Sprite(new Texture(Gdx.files.internal("data/GUI/pauseQuit.png")), 0, 0, 128, 69);
		main       = new Sprite(new Texture(Gdx.files.internal("data/GUI/mainMenu.png")), 0, 0, 128, 69);
	}

	public void update (float deltaTime)
	{
		if (Gdx.input.justTouched())
		{
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (OverlapTester.pointInRectangle(resumeBound, touchPoint.x, touchPoint.y))
			{				
				game.changeScreen(game.getPause(), game.getPlay());
				return;
			}
			
			if (OverlapTester.pointInRectangle(mainBound, touchPoint.x, touchPoint.y))
			{				
				game.newSure(true, this);
				game.changeScreen(game.getPause(), game.getSure());
				return;
			}
			
			if (OverlapTester.pointInRectangle(quitBound, touchPoint.x, touchPoint.y))
			{				
				game.newSure(false, this);
				game.changeScreen(this, game.getSure());
				return;
			}
		}
		
		Gdx.input.setInputProcessor(new InputAdapter()
		{
		    @Override
		    public boolean keyDown(int keycode)
		    {
		        if (keycode == Keys.ESCAPE)
		        {
					game.changeScreen(game.getPause(), game.getPlay());
					return true;
		        }
		        
		        return false;
		    }
		});
	}

	public void draw (float deltaTime)
	{
		guiCam.update();
		
		spriteBatch.setProjectionMatrix(guiCam.combined);
		
		spriteBatch.enableBlending();
		spriteBatch.begin();
		spriteBatch.draw(background, 195, -115);
		spriteBatch.draw(resume, 435, 265);
		spriteBatch.draw(main, 435, 185);
		spriteBatch.draw(quit, 435, 105);
		font.draw(spriteBatch, "Lives: " + game.getPlay().getRenderer().aria.getLives(), 290, 370);
		font.draw(spriteBatch, "CELESTIAL POWERS", 240, 330);
		
		int powerY = 310;
		
		if (game.getPlay().getRenderer().aria.getHasDoubleJump())
		{
			font.draw(spriteBatch, "Star Leap", 220, powerY);
			powerY = powerY - 20;
		}
		if(game.getAria().getHasWallJump())
		{
			font.draw(spriteBatch,  "Wall Jump", 220, powerY);
			powerY = powerY - 20;
		}
		spriteBatch.end();
	}

	@Override
	public void render (float delta)
	{
		update(delta);
		draw(delta);
	}

	//UNUSED OVERRIDES
	
	@Override
	public void resize (int width, int height)
	{
	}

	@Override
	public void show ()
	{
	}

	@Override
	public void hide () 
	{
	}

	@Override
	public void pause () 
	{
		
	}

	@Override
	public void resume ()
	{
	}

	@Override
	public void dispose ()
	{
	}
}
