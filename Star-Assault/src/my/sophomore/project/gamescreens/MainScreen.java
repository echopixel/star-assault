package my.sophomore.project.gamescreens;

import my.sophomore.project.StarAssault;
import my.sophomore.project.controllers.OverlapTester;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class MainScreen implements Screen
{
	private StarAssault game;

	private OrthographicCamera guiCam;
	
	private SpriteBatch spriteBatch;
	
	private Rectangle newBound;
	private Rectangle contBound;
	private Rectangle quitBound;
	private Rectangle debugBound;
	
	private Vector3 touchPoint;
	
	private Texture newGame;
	private Texture cont;
	private Texture quit;
	private Texture background;

	public MainScreen (StarAssault game)
	{
		this.game = game;

		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(400, 240, 0);
		
		spriteBatch = new SpriteBatch();
		
		newBound   = new Rectangle(300, 268, 200, 90);
		contBound  = new Rectangle(300, 158, 200, 90);
		quitBound  = new Rectangle(300, 48, 200, 90);
		debugBound = new Rectangle(700, 0, 100, 100);
		touchPoint = new Vector3();
		
		newGame    = new Texture(Gdx.files.internal("data/GUI/newGame.png"));
		cont       = new Texture(Gdx.files.internal("data/GUI/continue.png"));
		quit       = new Texture(Gdx.files.internal("data/GUI/quit.png"));
		background = new Texture(Gdx.files.internal("data/backgrounds/mainScreen.png"));
	}

	public void update (float deltaTime)
	{	
		if (Gdx.input.justTouched())
		{
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (OverlapTester.pointInRectangle(newBound, touchPoint.x, touchPoint.y))
			{
				game.newAria();
				game.newSelect();
				game.newList();
				game.changeScreen(game.getMain(), game.getSelect());
				return;
			}
			
			if (OverlapTester.pointInRectangle(contBound, touchPoint.x, touchPoint.y))
			{
				game.changeScreen(game.getMain(), game.getSelect());
				return;
			}
			
			if (OverlapTester.pointInRectangle(quitBound, touchPoint.x, touchPoint.y))
			{
				game.newSure(false, this);
				game.changeScreen(this, game.getSure());
				return;
			}
			
			if (OverlapTester.pointInRectangle(debugBound, touchPoint.x, touchPoint.y))
			{
				game.changeDebug();
				return;
			}
		}
		
		Gdx.input.setInputProcessor(new InputAdapter() 
		{
		    @Override
		    public boolean keyDown(int keycode) 
		    {
		        if (keycode == Keys.ESCAPE)
		        {
		        	game.newSure(false, game.getMain());
					game.changeScreen(game.getMain(), game.getSure());
					return true;
		        }
		        
		        return false;
		    }
		});
	}

	public void draw (float deltaTime)
	{		
		guiCam.update();
		
		spriteBatch.setProjectionMatrix(guiCam.combined);

		spriteBatch.enableBlending();
		spriteBatch.begin();
		spriteBatch.draw(background, 0, -32);
		spriteBatch.draw(newGame, 300, 230);
		spriteBatch.draw(cont, 300, 120);
		spriteBatch.draw(quit, 300, 10, 256, 128);
		
		spriteBatch.end();
	}

	@Override
	public void render (float delta) 
	{
		update(delta);
		draw(delta);
	}

	//UNUSED OVERRIDES
	
	@Override
	public void resize (int width, int height) 
	{
	}

	@Override
	public void show () 
	{
	}

	@Override
	public void hide ()
	{
	}

	@Override
	public void pause () 
	{
	}

	@Override
	public void resume () 
	{
	}

	@Override
	public void dispose () 
	{
	}
}
