package my.sophomore.project.gamescreens;

import my.sophomore.project.StarAssault;
import my.sophomore.project.controllers.OverlapTester;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class GameOverScreen implements Screen
{
	private StarAssault game;
	
	private OrthographicCamera guiCam;
	
	private Sprite main;
	private Sprite background;
	private SpriteBatch spriteBatch;
	
	private Rectangle mainBound;
	private Vector3 touchPoint;
	


	public GameOverScreen (StarAssault game)
	{		
		this.game = game;

		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(400, 240, 0);
		
		spriteBatch = new SpriteBatch();
		
		mainBound   = new Rectangle(50, 50, 128, 69);
		touchPoint  = new Vector3();
		
		background = new Sprite(new Texture(Gdx.files.internal(("data/backgrounds/facedownGOEffects.png"))), 0, 0, 800, 480);
		main       = new Sprite(new Texture(Gdx.files.internal("data/GUI/mainMenu.png")), 0, 0, 128, 69);
	}

	public void update (float deltaTime)
	{
		if (Gdx.input.justTouched())
		{
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (OverlapTester.pointInRectangle(mainBound, touchPoint.x, touchPoint.y))
			{		
				game.newAria();
				game.changeScreen(game.getOver(), game.getMain());
				return;
			}
		}
		
		Gdx.input.setInputProcessor(new InputAdapter()
		{
		    @Override
		    public boolean keyDown(int keycode)
		    {
		        if (keycode == Keys.ESCAPE)
		        {
		        	game.getAria().setLives(3);
					game.changeScreen(game.getOver(), game.getMain());
					return true;
		        }
		        
		        return false;
		    }
		});
	}

	public void draw (float deltaTime)
	{
		guiCam.update();
		
		spriteBatch.setProjectionMatrix(guiCam.combined);
		
		spriteBatch.enableBlending();
		spriteBatch.begin();
		spriteBatch.draw(background, 0, 0);
		spriteBatch.draw(main, 50, 50);
		spriteBatch.end();
	}

	@Override
	public void render (float delta)
	{
		update(delta);
		draw(delta);
	}

	//UNUSED OVERRIDES
	
	@Override
	public void resize (int width, int height)
	{
	}

	@Override
	public void show ()
	{
	}

	@Override
	public void hide () 
	{
	}

	@Override
	public void pause () 
	{
		
	}

	@Override
	public void resume ()
	{
	}

	@Override
	public void dispose ()
	{
	}
}
