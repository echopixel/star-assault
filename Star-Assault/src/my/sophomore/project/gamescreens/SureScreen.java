package my.sophomore.project.gamescreens;

import my.sophomore.project.StarAssault;
import my.sophomore.project.controllers.OverlapTester;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class SureScreen implements Screen
{
	private StarAssault game;
	
	private OrthographicCamera guiCam;
	
	private SpriteBatch spriteBatch;
	
	private Rectangle yesBound;
	private Rectangle noBound;
	
	private Vector3 touchPoint;
	
	private Sprite yes;
	private Sprite no;
	private Sprite background;
	
	private Screen screen;
	private Boolean bool;
	
	public SureScreen (StarAssault game, Screen screen, Boolean bool)
	{		
		this.game = game;
		this.screen = screen;
		this.bool = bool;
		
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(400, 240, 0);
		
		spriteBatch = new SpriteBatch();
		
		yesBound    = new Rectangle(260, 175, 128, 69);
		noBound     = new Rectangle(407, 175, 128, 69);
		touchPoint  = new Vector3();
		
		background = new Sprite(new Texture(Gdx.files.internal(("data/backgrounds/areYouSure.png"))), 0, 0, 305, 205);
		yes        = new Sprite(new Texture(Gdx.files.internal("data/GUI/yes.png")), 0, 0, 128, 69);
		no         = new Sprite(new Texture(Gdx.files.internal("data/GUI/no.png")), 0, 0, 128, 69);
	}

	public void update (float deltaTime) 
	{
		if (Gdx.input.justTouched()) 
		{
			guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

			if (OverlapTester.pointInRectangle(yesBound, touchPoint.x, touchPoint.y))
			{				
				if(bool == true)
				{
					game.changeScreen(game.getSure(), game.getMain());
					this.dispose();
				}
				else
				{
					Gdx.app.exit();
				}
				
				return;
			}
			
			if (OverlapTester.pointInRectangle(noBound, touchPoint.x, touchPoint.y)) 
			{			
				game.setScreen(screen);
				this.dispose();
				return;
			}
		}
		
		Gdx.input.setInputProcessor(new InputAdapter()
		{
		    @Override
		    public boolean keyDown(int keycode)
		    {
		        if (keycode == Keys.ESCAPE)
		        {
		        	game.setScreen(screen);
					game.getSure().dispose();
					return true;
		        }
		        
		        return false;
		    }
		});
		
	}

	public void draw (float deltaTime)
	{
		guiCam.update();
		
		spriteBatch.setProjectionMatrix(guiCam.combined);

		spriteBatch.enableBlending();
		spriteBatch.begin();
		spriteBatch.draw(background, 247, 138);
		spriteBatch.draw(yes, 260, 175);
		spriteBatch.draw(no, 407, 175);
		spriteBatch.end();
	}

	@Override
	public void render (float delta)
	{
		update(delta);
		draw(delta);
	}

	//UNUSED OVERRIDES
	
	@Override
	public void resize (int width, int height) 
	{
	}

	@Override
	public void show () 
	{
	}

	@Override
	public void hide () 
	{
	}

	@Override
	public void pause () 
	{
	}

	@Override
	public void resume () 
	{
	}

	@Override
	public void dispose () 
	{
	}
}
