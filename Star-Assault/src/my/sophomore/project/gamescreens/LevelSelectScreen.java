package my.sophomore.project.gamescreens;

import my.sophomore.project.StarAssault;
import my.sophomore.project.controllers.LevelSelectButton;
import my.sophomore.project.controllers.OverlapTester;
import my.sophomore.project.level.Level.Name;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class LevelSelectScreen implements Screen
{
	public enum LevelName
	{
		START, ONE, TWO, THREE, FOUR, FIVE, LAST, BLUE, GREEN, YELLOW, PURPLE, ORANGE
	}
	
	public enum Direction
	{
		UP, DOWN, LEFT, RIGHT, ENTER, ESCAPE, NONE
	}
	
	private StarAssault game;
	
	private OrthographicCamera guiCam;
	
	private Texture ariaTexture;
	private Sprite ariaSprite;
	private Sprite background;
	private SpriteBatch spriteBatch;
	private Sprite complete;
	private Sprite up;
	private Sprite down;
	private Sprite left;
	private Sprite right;
	
	private Rectangle upBound;
	private Rectangle downBound;
	private Rectangle leftBound;
	private Rectangle rightBound;
	
	private Vector3 touchPoint;
	
	private Direction controller;
	
	private LevelName lastLevel;
	private int ariaX;
	private int ariaY;
	private Direction moving;
	private int howFar;
	
	private LevelSelectButton start;
	private LevelSelectButton one;
	private LevelSelectButton two;
	private LevelSelectButton three;
	private LevelSelectButton four;
	private LevelSelectButton five;
	private LevelSelectButton last;
	private LevelSelectButton blue;
	private LevelSelectButton yellow;
	private LevelSelectButton green;
	private LevelSelectButton purple;
	private LevelSelectButton orange;
	
	private final int STARTX = 50;
	private final int STARTY = 54;
	
	private final int ONEX = 220;
	private final int ONEY = 188;
	
	private final int TWOX = 114;
	private final int TWOY = 188;
	
	private final int THREEX = 357;
	private final int THREEY = 188;
	
	private final int FOURX = 239;
	private final int FOURY = 355;
	
	private final int FIVEX = 530;
	private final int FIVEY = 89;
	
	private final int LASTX = 628;
	private final int LASTY = 422;
	
	private final int V = 7; //velocity
	
	public LevelSelectScreen (StarAssault game)
	{
		this.game = game;
		
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(400, 240, 0);
		
		upBound   = new Rectangle(675, 160, 32, 32);
		downBound  = new Rectangle(675, 76, 32, 32);
		leftBound  = new Rectangle(632, 118, 32, 32);
		rightBound = new Rectangle(717, 118, 32, 32);
		touchPoint = new Vector3();
				
		controller = Direction.NONE;
		
		ariaTexture = new Texture(Gdx.files.internal("data/sprites/aria.png"));
		ariaSprite = new Sprite(ariaTexture, 16, 0, 16, 16);
		background = new Sprite(new Texture(Gdx.files.internal(("data/backgrounds/galaxyMap.png"))), 0, 0, 800, 480);
		complete = new Sprite(new Texture(Gdx.files.internal(("data/backgrounds/levelComplete.png"))));
		up = new Sprite(new Texture(Gdx.files.internal(("data/GUI/up.png"))), 0, 0, 32, 32);
		down = new Sprite(new Texture(Gdx.files.internal(("data/GUI/down.png"))), 32, 32, 32, 32);
		left = new Sprite(new Texture(Gdx.files.internal(("data/GUI/left.png"))), 0, 32, 32, 32);
		right = new Sprite(new Texture(Gdx.files.internal(("data/GUI/right.png"))), 32, 0, 32, 32);
		
		spriteBatch = new SpriteBatch();
		
		ariaX = STARTX;
		ariaY = STARTY;
		
		start = new LevelSelectButton(STARTX, STARTY, LevelName.START);
		one = new LevelSelectButton(ONEX, ONEY, LevelName.ONE);
		two = new LevelSelectButton(TWOX, TWOY, LevelName.TWO);
		three = new LevelSelectButton(THREEX, THREEY, LevelName.THREE);
		four = new LevelSelectButton(FOURX, FOURY, LevelName.FOUR);
		five = new LevelSelectButton(FIVEX, FIVEY, LevelName.FIVE);
		last = new LevelSelectButton(LASTX, LASTY, LevelName.LAST);
		blue = new LevelSelectButton(ONEX, STARTY, LevelName.BLUE);
		purple = new LevelSelectButton(TWOX, FOURY, LevelName.PURPLE);
		yellow = new LevelSelectButton(THREEX, FOURY, LevelName.YELLOW);
		orange = new LevelSelectButton(THREEX, FIVEY, LevelName.ORANGE);
		green = new LevelSelectButton(FIVEX, LASTY, LevelName.GREEN);
		
		lastLevel = LevelName.START;
		
		moving = Direction.NONE;
	}

	public void setMove(LevelSelectButton select)
	{
		if(ariaX > select.getX())
		{
			moving = Direction.LEFT;
			howFar = select.getX();
		}
		
		if(ariaX < select.getX())
		{
			moving = Direction.RIGHT;
			howFar = select.getX();
		}
		
		if(ariaY > select.getY())
		{
			moving = Direction.DOWN;
			howFar = select.getY();
		}
		
		if(ariaY < select.getY())
		{
			moving = Direction.UP;
			howFar = select.getY();
		}
		
		lastLevel = select.getPlace();
	}
	
	
	public void update(float deltaTime)
	{
		if(game.getAndroid())
		{
			if (Gdx.input.justTouched())
			{
				guiCam.unproject(touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
	
				if (OverlapTester.pointInRectangle(upBound, touchPoint.x, touchPoint.y))
				{
					controller = Direction.UP;
					return;
				}
				
				if (OverlapTester.pointInRectangle(downBound, touchPoint.x, touchPoint.y))
				{
					controller = Direction.DOWN;
					return;
				}
				
				if (OverlapTester.pointInRectangle(leftBound, touchPoint.x, touchPoint.y))
				{
					controller = Direction.LEFT;
					return;
				}
				
				if (OverlapTester.pointInRectangle(rightBound, touchPoint.x, touchPoint.y))
				{
					controller = Direction.RIGHT;
					return;
				}
			}
		}
		
		
		Gdx.input.setInputProcessor(new InputAdapter()
		{
		    
			@Override
		    public boolean keyDown(int keycode)
			{
				if(keycode == Keys.D)
				{
					controller = Direction.RIGHT;
				}
				
				if(keycode == Keys.W)
				{
					controller = Direction.UP;
				}
				
				if(keycode == Keys.S)
				{
					controller = Direction.DOWN;
				}
				
				if(keycode == Keys.A)
				{
					controller = Direction.LEFT;
				}
				
				if(keycode == Keys.ENTER)
				{
					controller = Direction.ENTER;
				}
		    	
				if(keycode == Keys.ESCAPE)
				{
					controller = Direction.ESCAPE;
				}
		    	
		    	if(keycode == Keys.NUM_1 && game.getDebug())
		    	{
		    		if(game.getList().getLevelComplet(Name.ONE) == false)
		    		{
		    			game.getList().setLevelComplete(Name.ONE, true);
		    		}
		    		else
		    		{
		    			game.getList().setLevelComplete(Name.ONE, false);
		    		}
		    	}
		    	
		    	if(keycode == Keys.NUM_2 && game.getDebug())
		    	{
		    		if(game.getList().getLevelComplet(Name.TWO) == false)
		    		{
		    			game.getList().setLevelComplete(Name.TWO, true);
		    		}
		    		else
		    		{
		    			game.getList().setLevelComplete(Name.TWO, false);
		    		}
		    	}
		    	
		    	if(keycode == Keys.NUM_3 && game.getDebug())
		    	{
		    		if(game.getList().getLevelComplet(Name.THREE) == false)
		    		{
		    			game.getList().setLevelComplete(Name.THREE, true);
		    		}
		    		else
		    		{
		    			game.getList().setLevelComplete(Name.THREE, false);
		    		}
		    	}
		    	
		    	if(keycode == Keys.NUM_4 && game.getDebug())
		    	{
		    		if(game.getList().getLevelComplet(Name.FOUR) == false)
		    		{
		    			game.getList().setLevelComplete(Name.FOUR, true);
		    		}
		    		else
		    		{
		    			game.getList().setLevelComplete(Name.FOUR, false);
		    		}
		    	}
		    	
		    	if(keycode == Keys.NUM_5 && game.getDebug())
		    	{
		    		if(game.getList().getLevelComplet(Name.FIVE) == false)
		    		{
		    			game.getList().setLevelComplete(Name.FIVE, true);
		    		}
		    		else
		    		{
		    			game.getList().setLevelComplete(Name.FIVE, false);
		    		}
		    	}
		    	
		    	if(keycode == Keys.NUM_6 && game.getDebug())
		    	{
		    		if(game.getList().getLevelComplet(Name.LAST) == false)
		    		{
		    			game.getList().setLevelComplete(Name.LAST, true);
		    		}
		    		else
		    		{
		    			game.getList().setLevelComplete(Name.LAST, false);
		    		}
		    	}
		    	
		        return false;
		    }
			
			
		});
		controller();
		controller = Direction.NONE;
	}
	
	public void move()
	{
		if(moving == Direction.UP)
		{
			ariaY = ariaY + V;
			if(ariaY >= howFar)
			{
				ariaY = howFar;
				moving = Direction.NONE;
			}
		}
		
		if(moving == Direction.DOWN)
		{
			ariaY = ariaY - V;
			if(ariaY <= howFar)
			{
				ariaY = howFar;
				moving = Direction.NONE;
			}
		}
		
		if(moving == Direction.RIGHT)
		{
			ariaX = ariaX + V;
			if(ariaX >= howFar)
			{
				ariaX = howFar;
				moving = Direction.NONE;
			}
		}
		
		if(moving == Direction.LEFT)
		{
			ariaX = ariaX - V;
			if(ariaX <= howFar)
			{
				ariaX = howFar;
				moving = Direction.NONE;
			}
		}
	}
	
	public void draw()
	{
		guiCam.update();
		
		move();
		
		spriteBatch.enableBlending();
		spriteBatch.begin();
		spriteBatch.draw(background, 0, 0);
		if(game.getAndroid())
		{
			spriteBatch.draw(up, 675, 160);
			spriteBatch.draw(down, 675, 76);
			spriteBatch.draw(right, 717, 118);
			spriteBatch.draw(left, 632, 118);
		}
		
		
		if(game.getList().getLevelComplet(Name.ONE))
		{
			spriteBatch.draw(complete, one.getX()-1, one.getY()-30);
		}
		if(game.getList().getLevelComplet(Name.TWO))
		{
			spriteBatch.draw(complete, two.getX()-1, two.getY()-30);
		}
		if(game.getList().getLevelComplet(Name.THREE))
		{
			spriteBatch.draw(complete, three.getX()-1, three.getY()-30);
		}
		if(game.getList().getLevelComplet(Name.FOUR))
		{
			spriteBatch.draw(complete, four.getX()-1, four.getY()-30);
		}
		if(game.getList().getLevelComplet(Name.FIVE))
		{
			spriteBatch.draw(complete, five.getX()-1, five.getY()-30);
		}
		if(game.getList().getLevelComplet(Name.LAST))
		{
			spriteBatch.draw(complete, last.getX()-1, last.getY()-30);
		}
		spriteBatch.draw(ariaSprite, ariaX, ariaY, 32, 32);
		spriteBatch.end();
	}
	
	public void controller()
	{
		if(moving == Direction.NONE)
    	{
			if(lastLevel == LevelName.START)
			{
				if(controller == Direction.RIGHT)
    			{
		    		setMove(blue);
		    	}
			}
			
			if(lastLevel ==LevelName.ONE)
			{
				if(controller == Direction.DOWN)
				{
					setMove(blue);
				}
				
				if(controller == Direction.ENTER)
				{
					game.getAria().resetHP();
					game.getAria().resetMulti();
					game.newPlay(Name.ONE, game.getAria());
					game.changeScreen(game.getSelect(), game.getPlay());
				}
				
				if((controller == Direction.LEFT) && game.getList().getLevelComplet(Name.ONE))
				{
					setMove(two);
				}
				
				if((controller == Direction.RIGHT) && game.getList().getLevelComplet(Name.ONE))
				{
					setMove(three);
				}
			}
			
			if(lastLevel == LevelName.TWO)
			{
				if(controller == Direction.RIGHT)
				{
					setMove(one);
				}
				
				if((controller == Direction.UP) && (game.getList().getLevelComplet(Name.TWO) || game.getList().getLevelComplet(Name.FOUR)))
				{
					setMove(purple);
				}
				
				if(controller == Direction.ENTER)
				{
					game.getAria().resetHP();
					game.getAria().resetMulti();
					game.newPlay(Name.TWO, game.getAria());
					game.changeScreen(game.getSelect(), game.getPlay());
				}
			}
			
			if(lastLevel == LevelName.THREE)
			{
				if(controller == Direction.LEFT)
				{
					setMove(one);
				}
				
				if((controller == Direction.UP) && (game.getList().getLevelComplet(Name.THREE) || game.getList().getLevelComplet(Name.FOUR)))
				{
					setMove(yellow);
				}
				
				if((controller == Direction.DOWN) && game.getList().getLevelComplet(Name.THREE))
				{
					setMove(orange);
				}
				
				if(controller == Direction.ENTER)
				{
					game.getAria().resetHP();
					game.getAria().resetMulti();
					game.newPlay(Name.THREE, game.getAria());
					game.changeScreen(game.getSelect(), game.getPlay());
				}
			}
			
			if(lastLevel == LevelName.FOUR)
			{
				if((controller == Direction.LEFT) && (game.getList().getLevelComplet(Name.TWO) || game.getList().getLevelComplet(Name.FOUR)))
				{
					setMove(purple);
				}
				
				if((controller == Direction.RIGHT) && (game.getList().getLevelComplet(Name.THREE) || game.getList().getLevelComplet(Name.FOUR)))
				{
					setMove(yellow);
				}
				if(controller == Direction.ENTER)
				{
					game.getAria().resetHP();
					game.getAria().resetMulti();
					game.newPlay(Name.FOUR, game.getAria());
					game.changeScreen(game.getSelect(), game.getPlay());
				}
			}
			
			if(lastLevel == LevelName.FIVE)
			{
				if(controller == Direction.LEFT)
				{
					setMove(orange);
				}
				
				if((controller == Direction.UP)  && game.getList().getLevelComplet(Name.FIVE))
				{
					setMove(green);
				}
			}
			
			if(lastLevel == LevelName.LAST)
			{
				if(controller == Direction.LEFT)
				{
					setMove(green);
				}
			}
			
			if(lastLevel == LevelName.BLUE)
			{
				if(controller == Direction.LEFT)
				{
					setMove(start);
				}
				
				if(controller == Direction.UP)
				{
					setMove(one);
				}
			}
    		
			if(lastLevel == LevelName.GREEN)
			{
				if(controller == Direction.RIGHT)
				{
					setMove(last);
				}
				
				if(controller == Direction.DOWN)
				{
					setMove(five);
				}
			}
			
			if(lastLevel == LevelName.ORANGE)
			{
				if(controller == Direction.UP)
				{
					setMove(three);
				}
				
				if(controller == Direction.RIGHT)
				{
					setMove(five);
				}
			}
    		
			if(lastLevel == LevelName.PURPLE)
			{
				if(controller == Direction.RIGHT)
				{
					setMove(four);
				}
				
				if(controller == Direction.DOWN)
				{
					setMove(two);
				}
			}
    		
			if(lastLevel == LevelName.YELLOW)
			{
				if(controller == Direction.LEFT)
				{
					setMove(four);
				}
				
				if(controller == Direction.DOWN)
				{
					setMove(three);
				}
			}
    	}
		
    	if (controller == Direction.ESCAPE && moving == Direction.NONE)
        {
    		game.newSure(true, game.getSelect());
			game.changeScreen(game.getSelect(), game.getSure());
        }
	}
	
	@Override
	public void render(float delta)
	{
		draw();
		update(delta);
	}
	
	//UNUSED OVERIDES
	
	@Override
	public void dispose()
	{
	}

	@Override
	public void hide()
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resize(int arg0, int arg1)
	{
	}

	@Override
	public void resume()
	{
	}

	@Override
	public void show()
	{
	}
}
