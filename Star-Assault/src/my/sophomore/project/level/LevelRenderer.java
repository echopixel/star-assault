package my.sophomore.project.level;

import java.util.ArrayList;
import my.sophomore.project.StarAssault;
import my.sophomore.project.entity.AriaProjectile;
import my.sophomore.project.entity.Enemy;
import my.sophomore.project.entity.Octowalker;
import my.sophomore.project.entity.Player.Collision;
import my.sophomore.project.gamescreens.GamePlayScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.WorldManifold;

public class LevelRenderer extends Level implements ContactListener{
	
	/**
	 * The time the last frame was rendered, used for throttling framerate
	 */
	private long lastRender;
	
	private StarAssault game;
	
	private OrthographicCamera hudCam;
	private OrthographicCamera cam;
	private Vector3 tmp = new Vector3();
	private Texture star;
	
	private Sprite healthBar;
	private Sprite healthFrame;
	
	/** for debug rendering **/
	private boolean debugmode = false;
	private Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer();
	
	private Boolean moveAriaBody;
	private Boolean levelComplete;
	
	private ArrayList<Enemy> _activeOctowalkers;
	private ArrayList<Enemy> _destroyedEnemies;
	
	public LevelRenderer(Level level, StarAssault game) {
		super(game, game.getPlay().getLevelName(), game.getPlay().getLevel().aria);
		this.game = game;
		this.cam = level.cam;
		
		//Sets up the contact listener for custom collisions in the world.
		game.world.setContactListener(this);
				
		hudCam = new OrthographicCamera(800, 480);
		hudCam.position.set(400, 240, 0);
		healthFrame = new Sprite(new Texture(Gdx.files.internal("data/GUI/healthFrame.png")), 0, 0, 20, 200);
		healthBar = new Sprite(new Texture(Gdx.files.internal("data/GUI/healthBarPlain.png")), 0, 0, 20, 195);
		star = new Texture(Gdx.files.internal("data/sprites/star.png"));
		
		moveAriaBody = false;
		levelComplete = false;
		
		_activeOctowalkers = new ArrayList<Enemy>();
		if(game.getPlay().getLevelName() == Name.ONE)
		{
			Enemy enemy = new Octowalker(game.getWorld(), new Vector2(10f, 3.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(16f, 4.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(33.25f, 6.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(40f, 3.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(42f, 3.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(46f, 10.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(44f, 10.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(60f, 9.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(61.5f, 7.6f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(57.6f, 3f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(24f, 3f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(65.6f, 11.2f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(5.68f, 3f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(54.8f, 8.8f), this.game);
			_activeOctowalkers.add(enemy);
		}
		else if(game.getPlay().getLevelName() == Name.TWO)
		{
			Enemy enemy = new Octowalker(game.getWorld(), new Vector2(9f, 3.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(7f, 4.0f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(10f, 16.4f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(22.8f, 18.4f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(32.8f, 16f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(38f, 8.8f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(46.8f, 16.8f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(57.6f, 4.4f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(17.2f, 10.4f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(30.8f, 3f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(47.6f, 16.8f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(36.4f, 19.2f), this.game);
			_activeOctowalkers.add(enemy);
			enemy = new Octowalker(game.getWorld(), new Vector2(54f, 10.8f), this.game);
			_activeOctowalkers.add(enemy);
		}
		
		for (Enemy enemy : _activeOctowalkers)
		{
			System.out.println(enemy);
		}
		_destroyedEnemies = new ArrayList<Enemy>();
		
		Gdx.input.setInputProcessor(level.camController);
		
		font.setScale(2);
	}
	
	public void changeDebug()
	{
		if(debugmode == true)
		{
			debugmode = false;
		}
		else
		{
			debugmode = true;
		}
	}
	
	public void render () {
		long now = System.nanoTime();
		invincibleTime += Gdx.graphics.getDeltaTime();
			
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		aria.update(cam, game.getWorld());
		
		game.world.step(1.0f/60, 5, 5);
		//  Can't add bodies inside contact listener since it's during a step.
		// Will throw Assertion Failed! Error if you do.
		
		if (levelComplete == true)
		{
			levelComplete = false;
			game.changeScreen(this, game.getSelect());
		}
		
		for (Enemy enemy : _destroyedEnemies)
		{
			enemy.getBody().setActive(false);
		}
		
		for (AriaProjectile projectile : aria.getInactiveProjectiles())
		{
			projectile.getProjectileBody().setActive(false);
		}
		
		if (moveAriaBody)
		{
			this.aria.getBody().setTransform(this.aria.getSpawnPosition(), 0);
			moveAriaBody = false;
		}

		cam.zoom = 1f;
		cam.update();
		
		tileMapRenderer.getProjectionMatrix().set(cam.combined); //Not required when using tileMapRenderer.render(cam)
		tileMapRenderer.render(cam);// , layersList);

		//SPRITEBATCH
		spriteBatch.setProjectionMatrix(cam.combined);
		spriteBatch.begin();
		
		if(finishStar != null)
		{
			finishStar.draw(spriteBatch);
		}
		
		for (Enemy enemy : _activeOctowalkers)
		{
			enemy.draw(spriteBatch);
		}
		
		aria.draw(spriteBatch);
		
		spriteBatch.setProjectionMatrix(hudCam.projection);
		
		spriteBatch.draw(healthFrame, -380, 20);
		spriteBatch.draw(healthBar, -380, 23, 20, 195 * (aria.getHP()/20f));
		if(game.getAria().getMulti() >= 1)
		{
			spriteBatch.draw(star, -370, 195, 50, 50);
		}
		if(game.getAria().getMulti() >= 2)
		{
			spriteBatch.draw(star, -350, 195, 50, 50);
		}
		if(game.getAria().getMulti() >= 3)
		{
			spriteBatch.draw(star, -330, 195, 50, 50);
		}
		
		if(game.getAria().getInactiveProjectiles().size() >= 1)
		{
			spriteBatch.draw(star, -363, 190, 30, 30);
		}
		if(game.getAria().getInactiveProjectiles().size() >= 2)
		{
			spriteBatch.draw(star, -351, 190, 30, 30);
		}
		if(game.getAria().getInactiveProjectiles().size() >= 3)
		{
			spriteBatch.draw(star, -339, 190, 30, 30);
		}
		if(game.getAria().getInactiveProjectiles().size() >= 4)
		{
			spriteBatch.draw(star, -327, 190, 30, 30);
		}
		if(game.getAria().getInactiveProjectiles().size() >= 5)
		{
			spriteBatch.draw(star, -315, 190, 30, 30);
		}
		
		tmp.set(0, 0, 0);
		cam.unproject(tmp);
		
		spriteBatch.end();
		
		/**
		 * Draw this last, so we can see the collision boundaries on top of the
		 * sprites and map.
		 */	
		if(debugmode)
		{
			debugRenderer.render(game.getWorld(), cam.combined.scale(
				GamePlayScreen.PIXELS_PER_METER,
				GamePlayScreen.PIXELS_PER_METER,
				GamePlayScreen.PIXELS_PER_METER));
		}
		
		now = System.nanoTime();
		if (now - lastRender < 30000000)  // 30 ms, ~33FPS
		{
			try
			{
				Thread.sleep(30 - (now - lastRender) / 1000000);
			} 
			catch (InterruptedException e) 
			{
			}
		}

		lastRender = now;
	}
	
	/**
	 * @param contact
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#beginContact(com.badlogic.gdx.physics.box2d.Contact)
	 */
	@Override
	public void beginContact(Contact contact)
	{
		Fixture fixture1 = contact.getFixtureA();
		Fixture fixture2 = contact.getFixtureB();
		Object object1 = fixture1.getUserData();
		Object object2 = fixture2.getUserData();
		fixture1.getBody();
		fixture2.getBody();
		WorldManifold manifold = contact.getWorldManifold();
		manifold.getPoints();
		
		if (object1 == aria && object2 == getGroundBody())
		{
			if (manifold.getNormal().y > 0)
			{
				aria.setOnGround(true);
			}
		}
		else if (object2 == aria && object1 == getGroundBody())
		{
			if (manifold.getNormal().y > 0)
			{
				aria.setOnGround(true);
			}
		}
		
		if (object1 == aria && object2 == getBridgeBody())
		{
			if (manifold.getNormal().y > 0)
			{
				aria.setOnGround(true);
			}
		}
		else if (object2 == aria && object1 == getBridgeBody())
		{
			if (manifold.getNormal().y > 0)
			{
				aria.setOnGround(true);
			}
		}
		
		if (object1 == aria && object2 == getSpikeBody()) 
		{
			if (game.getAria().getLives() <= 0 && invincibleTime >= 1.5f)
			{
				game.setScreen(game.getOver());
			}
			else if (invincibleTime >= 1.5f) 
			{
				invincibleTime = 0;
				aria.setHP(-aria.getHP());
				moveAriaBody = true;
				
				aria.setHP(20);
				aria.setLives(-1);
				game.setScreen(game.getDied());
			}
		}
		else if (object1 == getSpikeBody() && object2 == aria) 
		{
			if (game.getAria().getLives() <= 0 && invincibleTime >= 1.5f)
			{
				game.setScreen(game.getOver());
			}
			else if (invincibleTime >= 1.5f) 
			{
				invincibleTime = 0;
				aria.setHP(-aria.getHP());
				moveAriaBody = true;
				aria.setHP(20);
				aria.setLives(-1);
				game.setScreen(game.getDied());
			}
		}
		
		if ((object1 == aria && object2 == finishStar) || (object2 == aria && object1 == finishStar))
		{
			game.getList().setLevelComplete(game.getPlay().getLevel().levelName, true);
			if(game.getPlay().getLevelName() == Name.ONE)
			{
				game.getAria().setDoubleJump(true);
			}
			else if(game.getPlay().getLevelName() == Name.TWO)
			{
				game.getAria().setWallJump(true);
			}
			levelComplete = true;
		}
		
		//Solves the projectile's collisions with a bunch of things.
		ProjectileCollisions(contact);
	}


	/**
	 * @param contact
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#beginContact(com.badlogic.gdx.physics.box2d.Contact)
	 */
	@Override
	public void endContact(Contact contact)
	{
		aria.setCanWallJump(false);
	}

	/**
	 * @param contact
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#beginContact(com.badlogic.gdx.physics.box2d.Contact)
	 */
	@Override
	public void preSolve(Contact contact, Manifold oldManifold)
	{
		Fixture fixture1 = contact.getFixtureA();
		Fixture fixture2 = contact.getFixtureB();
		Object object1 = fixture1.getUserData();
		Object object2 = fixture2.getUserData();
		fixture1.getBody();
		fixture2.getBody();
		WorldManifold manifold = contact.getWorldManifold();
		manifold.getPoints();
		
		//Solves the player's collision with enemy
		EnemyPlayerCollision(contact);
		
		if (object1 == aria && object2 == getBridgeBody())
		{
			if (manifold.getNormal().y < 0.3f) 
			{
				contact.setEnabled(false);
			}
		}
		else if (object2 == aria && object1 == getBridgeBody())
		{
			if (manifold.getNormal().y < 0.3f) 
			{
				contact.setEnabled(false);
			}
		}
		
		if((object1 == aria && object2 == getGroundBody()) || (object2 == aria && object1 == getGroundBody()))
		{
			if (manifold.getNormal().x > 0.2f && manifold.getNormal().y == 0f) 
			{
				aria.setCurrentCollision(Collision.LEFT);
				
				if (Gdx.input.isKeyPressed(Keys.A)) 
				{
					if (aria.getBody().getLinearVelocity().y <= aria.getWallSlideSpeed()
							&& aria.getHasWallJump() == true && aria.getDoWallJump() == false) 
					{
						aria.getBody().setLinearVelocity(aria.getBody().getLinearVelocity().x, aria.getWallSlideSpeed());
						aria.setCanWallJump(true);
						aria.setWallJumpNormal(manifold.getNormal());
					}
				}
			}
			else if (manifold.getNormal().x < -0.2f && manifold.getNormal().y == 0f) 
			{
				aria.setCurrentCollision(Collision.RIGHT);
				
				if (Gdx.input.isKeyPressed(Keys.D)) 
				{
					if (aria.getBody().getLinearVelocity().y <= aria.getWallSlideSpeed()
							&& aria.getHasWallJump() == true && aria.getDoWallJump() == false) 
					{
						aria.getBody().setLinearVelocity(aria.getBody().getLinearVelocity().x, aria.getWallSlideSpeed());
						aria.setCanWallJump(true);
						aria.setWallJumpNormal(manifold.getNormal());
					}
				}
			}

			else if (manifold.getNormal().y > 0f)
			{
				aria.setCurrentCollision(Collision.DOWN);
				aria.setCanWallJump(false);
			}
			else if (manifold.getNormal().y < 0 && manifold.getNormal().x == 0f)
			{
				aria.setCurrentCollision(Collision.UP);
				aria.setCanWallJump(false);
			}
		}
	}
	
		
		
		
		
	

	/**
	 * @param contact
	 * @see com.badlogic.gdx.physics.box2d.ContactListener#beginContact(com.badlogic.gdx.physics.box2d.Contact)
	 */
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		Fixture fixture1 = contact.getFixtureA();
		Fixture fixture2 = contact.getFixtureB();
		fixture1.getUserData();
		fixture2.getUserData();
		fixture1.getBody();
		fixture2.getBody();
		WorldManifold manifold = contact.getWorldManifold();
		manifold.getPoints();
		
		
		
	}
	
	public void EnemyPlayerCollision(Contact contact) {
		Fixture fixture1 = contact.getFixtureA();
		Fixture fixture2 = contact.getFixtureB();
		Object object1 = fixture1.getUserData();
		Object object2 = fixture2.getUserData();
		Body b1 = fixture1.getBody();
		Body b2 = fixture2.getBody();
		WorldManifold manifold = contact.getWorldManifold();
		manifold.getPoints();
		float x = manifold.getNormal().x;
		int o;
		for(o = 0; o < _activeOctowalkers.size(); o++) {
			if (object1 == aria && object2 == _activeOctowalkers.get(o))
			{
				if (invincibleTime >= 1.5f)
				{
					contact.setEnabled(true);
					contact.setEnabled(false);
					invincibleTime = 0;
					aria.setHit(true);
					aria.setHP(-_activeOctowalkers.get(o).getPower());
					b1.setLinearVelocity(new Vector2(0.5f, 0).mul(-x) );
					if (aria.getHP() <= 0 && game.getAria().getLives() <= 0)
					{
						game.setScreen(game.getOver());
					}
					else if (aria.getHP() <= 0)
					{
						
						aria.setHP(-aria.getHP()); // Sets her HP so the healthbar never goes below 0.
						// TODO Some kind of death animation and wait until it's done before doing the rest...
						aria.setHP(20);
						game.setScreen(game.getDied());
						moveAriaBody = true;
						aria.setLives(-1);
					}
				}
				else {
					contact.setEnabled(false);
				}
			}
		}
		
		for(o = 0; o < _activeOctowalkers.size(); o++) {
			if (object2 == aria && object1 == _activeOctowalkers.get(o)) {
				if (invincibleTime >= 1.5f) {
					contact.setEnabled(true);
					contact.setEnabled(false);
					invincibleTime = 0;
					aria.setHit(true);
					aria.setHP(-_activeOctowalkers.get(o).getPower());
					b2.setLinearVelocity(new Vector2(0.5f, 0).mul(-x) );
					if (aria.getHP() <= 0 && game.getAria().getLives() <= 0)
					{
						game.setScreen(game.getOver());
					}
					else if (aria.getHP() <= 0)
					{
						
						aria.setHP(-this.aria.getHP()); // Sets her HP so the healthbar never goes below 0.
						// TODO Some kind of death animation and wait until it's done before doing the rest...
						aria.setHP(20);
						game.setScreen(game.getDied());
						moveAriaBody = true;
						aria.setLives(-1);
					}
				}
				else {
					contact.setEnabled(false);
				}
			}
		}
	}
	
	public void collision()
	{
		
	}
	
	public void ProjectileCollisions(Contact contact) {
		Fixture fixture1 = contact.getFixtureA();
		Fixture fixture2 = contact.getFixtureB();
		Object object1 = fixture1.getUserData();
		Object object2 = fixture2.getUserData();
		fixture1.getBody();
		fixture2.getBody();
		WorldManifold manifold = contact.getWorldManifold();
		manifold.getPoints();
		//System.out.println("presolve between "+object1+" and "+object2);
		int i;
		int o;
		for(i = 0; i < aria.getProjectiles().size(); i++)
		{
			if (object1 == aria.getProjectiles().get(i) && (object2 == getBridgeBody() || object2 == getGroundBody())) {
				System.out.println("hit " + aria.getProjectiles().get(i).getId());
				//System.out.println(projectile.getId());
				
				aria.getInactiveProjectiles().add(aria.getProjectiles().remove(i));
				//NEed to set this body into a list to wait to be set to inactive.
				
				
			}
			else if (object2 == aria.getProjectiles().get(i) && (object1 == getBridgeBody() ||  object1 == getGroundBody())) {
				System.out.println("hit " + aria.getProjectiles().get(i).getId());
				//System.out.println(projectile.getId());
				aria.getInactiveProjectiles().add(aria.getProjectiles().remove(i));
			}	
		}
		
		contact.setEnabled(false);
		
		for(o = 0; o < _activeOctowalkers.size(); o++) {
			for(i = 0; i < aria.getProjectiles().size(); i++) {	
				if (object1 == aria.getProjectiles().get(i) && object2 == _activeOctowalkers.get(o)) {
					aria.getInactiveProjectiles().add(aria.getProjectiles().remove(i));
					_activeOctowalkers.get(o).setHP(-aria.getPower());
					System.out.println(_activeOctowalkers.get(o).getHP());
					if (_activeOctowalkers.get(o).getHP() <= 0) {
						_destroyedEnemies.add(_activeOctowalkers.remove(o));
					}
				}
				else if (object2 == aria.getProjectiles().get(i) && object1 == _activeOctowalkers.get(o)) {
					aria.getInactiveProjectiles().add(aria.getProjectiles().remove(i));
					_activeOctowalkers.get(o).setHP(-aria.getPower());
					System.out.println(_activeOctowalkers.get(o).getHP());
					if (_activeOctowalkers.get(o).getHP() <= 0) {
						_destroyedEnemies.add(_activeOctowalkers.remove(o));
					}
				}
			}
		}
	}
}
