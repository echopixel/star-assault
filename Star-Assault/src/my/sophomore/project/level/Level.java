package my.sophomore.project.level;

import java.util.ArrayList;
import java.util.HashMap;

import my.sophomore.project.StarAssault;
import my.sophomore.project.controllers.OrthoCamController;
import my.sophomore.project.entity.FinishStar;
import my.sophomore.project.entity.Player;
import my.sophomore.project.gamescreens.GamePlayScreen;
import my.sophomore.project.gamescreens.MainScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TileMapRenderer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObject;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObjectGroup;
import com.badlogic.gdx.math.Vector2;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class Level extends MainScreen {
	
	public enum Name
	{
		ONE, TWO, THREE, FOUR, FIVE, LAST
	}
	
	SpriteBatch spriteBatch;
	BitmapFont font;
	
	GamePlayScreen screen;
	StarAssault game;
	
	TileMapRenderer tileMapRenderer;
	TiledMap map;
	SimpleTileAtlas atlas;
	
	Name levelName;
	protected FinishStar finishStar;

	public Player aria;
	
	OrthographicCamera cam;
	OrthographicCamera parallax;
	OrthographicCamera background;
	OrthoCamController camController;
	public Vector2 maxCamPosition = new Vector2(0, 0);
	
	public float invincibleTime = 0;
	
	private Body spikeBody;
	private Body groundBody;
	private Body bridgeBody;
	
	public Level(StarAssault game, Name levelName, Player aria) {
		super(game);
		this.levelName = levelName;
		this.game = game;
		this.aria = aria;
		
		/**
		 * You can set the world's gravity in its constructor. Here, the gravity
		 * is negative in the y direction (as in, pulling things down).
		 */
		if (game.getWorld() != null && !game.getWorld().isLocked())
		{
			game.getWorld().dispose();
			game.newWorld();
		}
		this.aria.setWorld(game.getWorld());
		
		createDemoLevel();
	}
	
	public GamePlayScreen getScreen()
	{
		return screen;
	}
	
	public void createDemoLevel () {
		
		int i;
		long startTime, endTime;
		font = new BitmapFont();
		font.setColor(Color.RED);

		spriteBatch = new SpriteBatch();

		final String path = "data/tiledmap/";
		
		String mapName = null;

		if(levelName == Name.ONE)
		{
			mapName = "level1";
			finishStar = new FinishStar(game.getWorld(), game, new Vector2(77.0f, 4.0f));

			game.getAria().setPlayerSpawn(game.getWorld(), new Vector2(2.0f, 3.0f));

		}
		
		if(levelName == Name.TWO)
		{
			mapName = "level2";
			finishStar = new FinishStar(game.getWorld(), game, new Vector2(59.0f, 2.5f));
			
			game.getAria().setPlayerSpawn(game.getWorld(), new Vector2(15.0f, 2f));

		}
		
		if(levelName == Name.THREE)
		{
			mapName = "level3";
			finishStar = new FinishStar(game.getWorld(), game, new Vector2(78.4f, 30.0f));
			
			game.getAria().setPlayerSpawn(game.getWorld(), new Vector2(2.0f, 77.5f));

		}
		
		if(levelName == Name.FOUR)
		{
			mapName = "level4";
			finishStar = new FinishStar(game.getWorld(), game, new Vector2(17.6f, 4.0f));

			game.getAria().setPlayerSpawn(game.getWorld(), new Vector2(2.0f, 3.0f));

		}
		
		FileHandle mapHandle = Gdx.files.internal(path + mapName + ".tmx");
		FileHandle baseDir = Gdx.files.internal(path);

		startTime = System.currentTimeMillis();
		map = TiledLoader.createMap(mapHandle);
		endTime = System.currentTimeMillis();
		System.out.println("Loaded map in " + (endTime - startTime) + "mS");

		atlas = new SimpleTileAtlas(map, baseDir);

		int blockWidth = 400;
		int blockHeight = 400;

		startTime = System.currentTimeMillis();

		tileMapRenderer = new TileMapRenderer(map, atlas, blockWidth, blockHeight, 4, 4);
		
		endTime = System.currentTimeMillis();
		System.out.println("Created cache in " + (endTime - startTime) + "mS");

		for (TiledObjectGroup group : map.objectGroups) {
			for (TiledObject object : group.objects) {
				// TODO: Draw sprites where objects occur
				System.out.println("Object " + object.name + " x,y = " + object.x + "," + object.y + " width,height = "
					+ object.width + "," + object.height);
			}
		}

		float aspectRatio = (float)Gdx.graphics.getWidth() / (float)Gdx.graphics.getHeight();
		cam = new OrthographicCamera(100f * aspectRatio, 100f);

		cam.position.set(100, 50, 0);
		camController = new OrthoCamController(cam);

		loadCollisions("data/tiledmap/collisions.txt", game.getWorld(),
				40f);
		

		maxCamPosition.set(tileMapRenderer.getMapWidthUnits(), tileMapRenderer.getMapHeightUnits());
		
		
		
	}

	
	
	/**
	 * Reads a file describing the collision boundaries that should be set
	 * per-tile and adds static bodies to the boxd world.
	 * 
	 * @param collisionsFile
	 * @param world
	 * @param pixelsPerMeter
	 *            the pixels per meter scale used for this world
	 */
	public void loadCollisions(String collisionsFile, World world,
			float pixelsPerMeter) {
		/**
		 * Detect the tiles and dynamically create a representation of the map
		 * layout, for collision detection. Each tile has its own collision
		 * rules stored in an associated file.
		 * 
		 * The file contains lines in this format (one line per type of tile):
		 * tileNumber XxY,XxY XxY,XxY
		 * 
		 * Ex:
		 * 
		 * 3 0x0,31x0 ... 4 0x0,29x0 29x0,29x31
		 * 
		 * For a 32x32 tileset, the above describes one line segment for tile #3
		 * and two for tile #4. Tile #3 has a line segment across the top. Tile
		 * #1 has a line segment across most of the top and a line segment from
		 * the top to the bottom, 30 pixels in.
		 */

		FileHandle fh = Gdx.files.internal(collisionsFile);
		String collisionFile = fh.readString();
		String lines[] = collisionFile.split("\\r?\\n");

		HashMap<Integer, ArrayList<LineSegment>> tileCollisionJoints = new HashMap<Integer, ArrayList<LineSegment>>();
		/**
		 * Some locations on the map (perhaps most locations) are "undefined",
		 * empty space, and will have the tile type 0. This code adds an empty
		 * list of line segments for this "default" tile.
		 */
		tileCollisionJoints.put(Integer.valueOf(0),
				new ArrayList<LineSegment>());

		for (int n = 0; n < lines.length; n++) {
			String cols[] = lines[n].split(" ");
			int tileNo = Integer.parseInt(cols[0]);

			ArrayList<LineSegment> tmp = new ArrayList<LineSegment>();

			for (int m = 1; m < cols.length; m++) {
				String coords[] = cols[m].split(",");

				String start[] = coords[0].split("x");
				String end[] = coords[1].split("x");

				tmp.add(new LineSegment(Integer.parseInt(start[0]), Integer
						.parseInt(start[1]), Integer.parseInt(end[0]), Integer
						.parseInt(end[1])));
			}

			tileCollisionJoints.put(Integer.valueOf(tileNo), tmp);
		}

		ArrayList<LineSegment> collisionLineSegments = new ArrayList<LineSegment>();
		ArrayList<LineSegment> spikeLineSegments = new ArrayList<LineSegment>();
		ArrayList<LineSegment> bridgeLineSegments = new ArrayList<LineSegment>();

		for (int y = 0; y < getMap().height; y++) {
			for (int x = 0; x < getMap().width; x++) {
				/*
				 * LAYER 2 IS SPIKES
				 * Layers go from bottom to top.  0, 1, 2, 3, 4...
				 */
				int spikeLayer = getMap().layers.get(2).tiles[(getMap().height - 1)
				                                              	- y][x];
				int groundLayer = getMap().layers.get(3).tiles[(getMap().height - 1)
						- y][x];
				int bridgeLayer = getMap().layers.get(4).tiles[(getMap().height - 1)
				                    						- y][x];
				
				// ITERATES THROUGH THE BRIDGE LAYER
				for (int n = 0; n < tileCollisionJoints.get(
						Integer.valueOf(bridgeLayer)).size(); n++) {

					LineSegment lineSeg = tileCollisionJoints.get(
							Integer.valueOf(bridgeLayer)).get(n);

					addOrExtendCollisionLineSegment(x * getMap().tileWidth
							+ lineSeg.start().x, y * getMap().tileHeight
							- lineSeg.start().y + getMap().tileHeight, x
							* getMap().tileWidth + lineSeg.end().x, y
							* getMap().tileHeight - lineSeg.end().y
							+ getMap().tileHeight, bridgeLineSegments);
					
					
				}
				
				//ITERATES THROUGH THE GROUND LAYER
				for (int o = 0; o < tileCollisionJoints.get(
						Integer.valueOf(groundLayer)).size(); o++) {
					
					LineSegment lineSeg2 = tileCollisionJoints.get(
							Integer.valueOf(groundLayer)).get(o);
					
					addOrExtendCollisionLineSegment(x * getMap().tileWidth
							+ lineSeg2.start().x, y * getMap().tileHeight
							- lineSeg2.start().y + getMap().tileHeight, x
							* getMap().tileWidth + lineSeg2.end().x, y
							* getMap().tileHeight - lineSeg2.end().y
							+ getMap().tileHeight, collisionLineSegments);
				}
				
				// ITERATES THROUGH THE SPIKE LAYER
				for (int n = 0; n < tileCollisionJoints.get(
						Integer.valueOf(spikeLayer)).size(); n++) {

					LineSegment lineSegSpike = tileCollisionJoints.get(
							Integer.valueOf(spikeLayer)).get(n);

					addOrExtendCollisionLineSegment(x * getMap().tileWidth
							+ lineSegSpike.start().x, y * getMap().tileHeight
							- lineSegSpike.start().y + getMap().tileHeight, x
							* getMap().tileWidth + lineSegSpike.end().x, y
							* getMap().tileHeight - lineSegSpike.end().y
							+ getMap().tileHeight, spikeLineSegments);
				}
			}
		}
		
		BodyDef groundBodyDef = new BodyDef();
		groundBodyDef.type = BodyDef.BodyType.StaticBody;
		groundBody = world.createBody(groundBodyDef);
		for (LineSegment lineSegment : collisionLineSegments) {
			EdgeShape environmentShape = new EdgeShape();
			
			environmentShape.set(
					lineSegment.start().mul(1 / pixelsPerMeter), lineSegment
							.end().mul(1 / pixelsPerMeter));
			FixtureDef groundBodyFixtureDef = new FixtureDef();
			groundBodyFixtureDef.shape = environmentShape;
			groundBodyFixtureDef.density = 0;
			groundBodyFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_SCENERY;
			groundBodyFixtureDef.filter.maskBits = GamePlayScreen.MASK_SCENERY;
			Fixture groundFixture = groundBody.createFixture(groundBodyFixtureDef);
			groundFixture.setUserData(groundBody);
			environmentShape.dispose();
		}	
			BodyDef spikeBodyDef = new BodyDef();
			spikeBodyDef.type = BodyDef.BodyType.StaticBody;
			spikeBody = world.createBody(spikeBodyDef);
			for (LineSegment lineSegmentSpike : spikeLineSegments) {
				EdgeShape spikeShape = new EdgeShape();
				
				spikeShape.set(
						lineSegmentSpike.start().mul(1 / pixelsPerMeter), lineSegmentSpike
								.end().mul(1 / pixelsPerMeter));
				FixtureDef spikeBodyFixtureDef = new FixtureDef();
				spikeBodyFixtureDef.shape = spikeShape;
				spikeBodyFixtureDef.density = 0;
				spikeBodyFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_SCENERY;
				spikeBodyFixtureDef.filter.maskBits = GamePlayScreen.MASK_SCENERY;
				Fixture spikeFixture = spikeBody.createFixture(spikeBodyFixtureDef);
				spikeFixture.setUserData(spikeBody);
				spikeShape.dispose();
		}
			BodyDef bridgeBodyDef = new BodyDef();
			bridgeBodyDef.type = BodyDef.BodyType.StaticBody;
			bridgeBody = world.createBody(spikeBodyDef);
			for (LineSegment lineSegmentBridge : bridgeLineSegments) {
				EdgeShape bridgeShape = new EdgeShape();
				
				bridgeShape.set(
						lineSegmentBridge.start().mul(1 / pixelsPerMeter), lineSegmentBridge
								.end().mul(1 / pixelsPerMeter));
				FixtureDef bridgeBodyFixtureDef = new FixtureDef();
				bridgeBodyFixtureDef.shape = bridgeShape;
				bridgeBodyFixtureDef.density = 0;
				bridgeBodyFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_SCENERY;
				bridgeBodyFixtureDef.filter.maskBits = GamePlayScreen.MASK_SCENERY;
				Fixture bridgeFixture = bridgeBody.createFixture(bridgeBodyFixtureDef);
				bridgeFixture.setUserData(bridgeBody);
				bridgeShape.dispose();
		}
		
		/**
		 * Drawing a boundary around the entire map. We can't use a box because
		 * then the world objects would be inside and the physics engine would
		 * try to push them out.
		 */

		EdgeShape mapBounds = new EdgeShape();
		FixtureDef mapFixtureDef = new FixtureDef();
		mapFixtureDef.filter.categoryBits = GamePlayScreen.CATEGORY_SCENERY;
		mapFixtureDef.filter.maskBits = GamePlayScreen.MASK_SCENERY;
		mapFixtureDef.density = 0;
		mapFixtureDef.shape = mapBounds;
		mapBounds.set(new Vector2(0.0f, 0.0f), new Vector2(getWidth()
				/ pixelsPerMeter, 0.0f));
		Fixture bound1Fixture = groundBody.createFixture(mapFixtureDef);
		bound1Fixture.setUserData(groundBody);

		mapBounds.set(new Vector2(0.0f, (getHeight() + 200) / pixelsPerMeter),
				new Vector2(getWidth() / pixelsPerMeter, (getHeight() + 200)
						/ pixelsPerMeter));
		Fixture bound2Fixture = groundBody.createFixture(mapFixtureDef);
		bound2Fixture.setUserData(groundBody);

		mapBounds.set(new Vector2(0.0f, 0.0f), new Vector2(0.0f,
				getHeight() / pixelsPerMeter));
		Fixture bound3Fixture = groundBody.createFixture(mapFixtureDef);
		bound3Fixture.setUserData(groundBody);

		mapBounds.set(new Vector2((getWidth()) / pixelsPerMeter, 0.0f),
				new Vector2((getWidth()) / pixelsPerMeter, getHeight()
						/ pixelsPerMeter));
		Fixture bound4Fixture = groundBody.createFixture(mapFixtureDef);
		bound4Fixture.setUserData(groundBody);

		mapBounds.dispose();
	}

	/**
	 * This is a helper function that makes calls that will attempt to extend
	 * one of the line segments already tracked by TiledMapHelper, if possible.
	 * The goal is to have as few line segments as possible.
	 * 
	 * Ex: If you have a line segment in the system that is from 1x1 to 3x3 and
	 * this function is called for a line that is 4x4 to 9x9, rather than add a
	 * whole new line segment to the list, the 1x1,3x3 line will be extended to
	 * 1x1,9x9. See also: LineSegment.extendIfPossible.
	 * 
	 * @param lsx1
	 *            starting x of the new line segment
	 * @param lsy1
	 *            starting y of the new line segment
	 * @param lsx2
	 *            ending x of the new line segment
	 * @param lsy2
	 *            ending y of the new line segment
	 * @param collisionLineSegments
	 *            the current list of line segments
	 */
	private void addOrExtendCollisionLineSegment(float lsx1, float lsy1,
			float lsx2, float lsy2, ArrayList<LineSegment> collisionLineSegments) {
		LineSegment line = new LineSegment(lsx1, lsy1, lsx2, lsy2);

		boolean didextend = false;

		for (LineSegment test : collisionLineSegments) {
			if (test.extendIfPossible(line)) {
				didextend = true;
				break;
			}
		}

		if (!didextend) {
			collisionLineSegments.add(line);
		}
	}
	
	/**
	 * Describes the start and end points of a line segment and contains a
	 * helper method useful for extending line segments.
	 */
	private class LineSegment {
		private Vector2 start = new Vector2();
		private Vector2 end = new Vector2();

		/**
		 * Construct a new LineSegment with the specified coordinates.
		 * 
		 * @param x1
		 * @param y1
		 * @param x2
		 * @param y2
		 */
		public LineSegment(float x1, float y1, float x2, float y2) {
			start = new Vector2(x1, y1);
			end = new Vector2(x2, y2);
		}

		/**
		 * The "start" of the line. Start and end are misnomers, this is just
		 * one end of the line.
		 * 
		 * @return Vector2
		 */
		public Vector2 start() {
			return start;
		}

		/**
		 * The "end" of the line. Start and end are misnomers, this is just one
		 * end of the line.
		 * 
		 * @return Vector2
		 */
		public Vector2 end() {
			return end;
		}

		/**
		 * Determine if the requested line could be tacked on to the end of this
		 * line with no kinks or gaps. If it can, the current LineSegment will
		 * be extended by the length of the passed LineSegment.
		 * 
		 * @param lineSegment
		 * @return boolean true if line was extended, false if not.
		 */
		public boolean extendIfPossible(LineSegment lineSegment) {
			/**
			 * First, let's see if the slopes of the two segments are the same.
			 */
			double slope1 = Math.atan2(end.y - start.y, end.x - start.x);
			double slope2 = Math.atan2(lineSegment.end.y - lineSegment.start.y,
					lineSegment.end.x - lineSegment.start.x);

			if (Math.abs(slope1 - slope2) > 1e-9) {
				return false;
			}

			/**
			 * Second, check if either end of this line segment is adjacent to
			 * the requested line segment. So, 1 pixel away up through sqrt(2)
			 * away.
			 * 
			 * Whichever two points are within the right range will be "merged"
			 * so that the two outer points will describe the line segment.
			 */
			if (start.dst(lineSegment.start) <= Math.sqrt(2) + 1e-9) {
				start.set(lineSegment.end);
				return true;
			} else if (end.dst(lineSegment.start) <= Math.sqrt(2) + 1e-9) {
				end.set(lineSegment.end);
				return true;
			} else if (end.dst(lineSegment.end) <= Math.sqrt(2) + 1e-9) {
				end.set(lineSegment.start);
				return true;
			} else if (start.dst(lineSegment.end) <= Math.sqrt(2) + 1e-9) {
				start.set(lineSegment.start);
				return true;
			}

			return false;
		}

		/**
		 * Returns a pretty description of the LineSegment.
		 * 
		 * @return String
		 */
		@Override
		public String toString() {
			return "[" + start.x + "x" + start.y + "] -> [" + end.x + "x"
					+ end.y + "]";
		}
	}
	
	public Player getPlayer()
	{
		return aria;
	}
	
	/**
	 * Get the height of the map in pixels
	 * 
	 * @return y
	 */
	public int getHeight() {
		return map.height * map.tileHeight;
	}

	/**
	 * Get the width of the map in pixels
	 * 
	 * @return x
	 */
	public int getWidth() {
		return map.width * map.tileWidth;
	}

	/**
	 * Get the map, useful for iterating over the set of tiles found within
	 * 
	 * @return TiledMap
	 */
	public TiledMap getMap() {
		return map;
	}

	/**
	 * Calls dispose on all disposable resources held by this object.
	 */
	public void dispose(World world) {
		while (world.isLocked()) {
			System.out.println("locked");
		}
		if(!world.isLocked()) {
		atlas.dispose();
		tileMapRenderer.dispose();
		}
	}
	
	public Body getSpikeBody () {
		return spikeBody;
	}
	
	public Body getGroundBody() {
		return groundBody;
	}
	
	public Body getBridgeBody() {
		return bridgeBody;
	}

}
